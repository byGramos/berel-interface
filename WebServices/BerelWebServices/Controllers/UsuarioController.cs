﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Movil
    /// </summary>
    [RoutePrefix("api/Usuario")]
    public class UsuarioController : ApiController
    {
        [HttpGet]
        [Route("test")]
        public string Hello()
        {
            return "Hello!!!";
        }

        [Route("IniciarSesion")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage IniciarSesion([FromBody]Models.Login login)
        {

            Response<Models.Usuario> resp = Models.Usuario.iniciarSesion(login.Usuario,login.Password,login.VersionApp);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<Models.Usuario>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        
        [Route("ObtenerPromotores")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerPromotores([FromBody]Models.Usuario usuario)
        {

            Response<List<Models.Usuario>> resp = Models.Usuario.ObtenerPromotores(usuario.ID);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.Usuario>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [HttpPost]
        [Route("Web/Guardar")]
        public HttpResponseMessage GuardarUsuario([FromBody]Models.Usuario usuario)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Response<bool> resp = Models.Usuario.GuardarUsuario(usuario);

            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }


        [HttpGet]
        [Route("Web/ObtenerUsuarios")]
        public HttpResponseMessage ObtenerUsuarios()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            string fechainicio = Utiles.GetQueryParamOrNull(Request, "fecha_inicio");
            string fechafin = Utiles.GetQueryParamOrNull(Request, "fecha_fin");
            int? idsupervisor = Utiles.GetQueryParamOrNull<int>(Request, "idSupervisor");
            int? idagencia = Utiles.GetQueryParamOrNull<int>(Request, "idAgencia");
            int? estatus = Utiles.GetQueryParamOrNull<int>(Request, "activo");

            DateTime fechaInicioConv;
            DateTime fechaFinConv;

            if (fechainicio != null && fechafin != null
                && DateTime.TryParse(fechainicio, Utiles.FormatDateProvider, System.Globalization.DateTimeStyles.None, out fechaInicioConv)
                && DateTime.TryParse(fechafin, Utiles.FormatDateProvider, System.Globalization.DateTimeStyles.None, out fechaFinConv))
            {
                Response<List<Models.Usuario>> resp = Models.Usuario.ObtenerUsuarios(fechaInicioConv, fechaFinConv, idsupervisor, idagencia, estatus);

                message.Content = new ObjectContent<Response<List<Models.Usuario>>>(resp, Utiles.Formatter);
            }
            else
            {
                message.StatusCode = HttpStatusCode.BadRequest;

                message.Content = new ObjectContent<Response<object>>(new Response<object>()
                {
                    Estatus = Estatus.Error,
                    Mensaje = "No se recibio algun campo requerido o con el formato correcto. Fecha: yyyy-mm-dd",
                    MensajeTecnico = string.Format("FechaInicio={1}, FechaInicio={2}", fechainicio, fechafin)
                }, Utiles.Formatter);
            }


            return message;
        }

        [HttpGet]
        [Route("Web/ObtenerUsuario")]
        public HttpResponseMessage ObtenerUsuario()
        {           

            int? idUsuario = Utiles.GetQueryParamOrNull<int>(Request, "idUsuario");

            Response<Models.Usuario> usuario = Models.Usuario.ObtenerUsuario(idUsuario);
            HttpResponseMessage resp = new HttpResponseMessage();

            resp.Content = new ObjectContent<Response<Models.Usuario>>(usuario, Utiles.Formatter);

            if (usuario.Estatus == Estatus.Exito)
            {
                resp.StatusCode = HttpStatusCode.OK;
            }
            else if (usuario.Estatus == Estatus.Advertencia)
            {
                resp.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }

            return resp;
        }


        [HttpPost]
        [Route("Web/Actualizar")]
        public HttpResponseMessage ActualizarUsuario([FromBody]Models.Usuario usuario)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Response<bool> resp = Models.Usuario.ActualizarUsuario(usuario);

            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [HttpPost]
        [Route("Web/ActivarUsuario")]
        public HttpResponseMessage ActivarUsuario([FromBody]Models.Usuario usuario)
        {

            /*int? idUsuario = Utiles.GetQueryParamOrNull<int>(Request, "idUsuario");
            bool? activo = Utiles.GetQueryParamOrNull<bool>(Request, "activo");
            string fechaBaja = Utiles.GetQueryParamOrNull(Request, "fechaBaja");*/

            //DateTime fechaBajaConv;

            //DateTime.TryParse(fechaBaja, Utiles.FormatDateProvider, System.Globalization.DateTimeStyles.None, out fechaBajaConv);

            HttpResponseMessage message = new HttpResponseMessage();

            
            Response<bool> resp = Models.Usuario.ActivarUsuario(usuario.ID, usuario.EstatusPromotor, usuario.FechaBaja);
            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;

        }


    }
}
