﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Movil
    /// </summary>
    [RoutePrefix("api/Tienda")]
    public class TiendaController : ApiController
    {

        [HttpGet]
        [Route("test")]
        public string Hello()
        {
            return "Hello!!!";
        }

        [Route("ObtenerTiendas")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerTiendas()
        {

            Response<List<Models.Tienda>> resp = Models.Tienda.ObtenerTiendas();

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.Tienda>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [Route("ObtenerTransferencias")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerTransferencias([FromBody]Models.Usuario usuario)
        {

            Response<List<Models.Tienda>> resp = Models.Tienda.ObtenerTransferencias(usuario.ID);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.Tienda>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }


        [Route("ObtenerTiendasAsignadas")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerTiendasAsignadas([FromBody]Models.Usuario usuario)
        {

            Response<List<Models.Tienda>> resp = Models.Tienda.ObtenerTiendasAsignadas(usuario.ID);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.Tienda>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }



    }
}
