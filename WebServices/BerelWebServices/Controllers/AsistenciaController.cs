﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Web
    /// </summary>
    [RoutePrefix("api/Asistencia")]
    public class AsistenciaController : ApiController
    {
        [HttpGet]
        [Route("Web/ObtenerAsistencia")]
        public HttpResponseMessage ObtenerAsistencia()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            string fechainicio = Utiles.GetQueryParamOrNull(Request, "fecha_inicio");
            string fechafin = Utiles.GetQueryParamOrNull(Request, "fecha_fin");

            DateTime fechaInicioConv;
            DateTime fechaFinConv;

            if (fechainicio != null && fechafin != null
                && DateTime.TryParse(fechainicio, Utiles.FormatDateProvider, System.Globalization.DateTimeStyles.None, out fechaInicioConv)
                && DateTime.TryParse(fechafin, Utiles.FormatDateProvider, System.Globalization.DateTimeStyles.None, out fechaFinConv))
            {
                Response<List<Models.Asistencia>> resp = Models.Asistencia.ObtenerAsistencia(fechaInicioConv, fechaFinConv);

                message.Content = new ObjectContent<Response<List<Models.Asistencia>>>(resp, Utiles.Formatter);
            }
            else
            {
                message.StatusCode = HttpStatusCode.BadRequest;

                message.Content = new ObjectContent<Response<object>>(new Response<object>()
                {
                    Estatus = Estatus.Error,
                    Mensaje = "No se recibio algun campo requerido o con el formato correcto. Fecha: yyyy-mm-dd",
                    MensajeTecnico = string.Format("idTipoTrabajador={0}, FechaInicio={1}, FechaInicio={2}", fechainicio, fechafin)
                }, Utiles.Formatter);
            }


            return message;
        }

        [HttpPost]
        [Route("Web/ReportarIncidencia")]
        public HttpResponseMessage ReportarIncidencia([FromBody]Models.Asistencia asistencia)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Response<bool> resp = Models.Asistencia.ReportarIncidencia(asistencia);
            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [HttpPost]
        [Route("Web/AprobarIncidencia")]
        public HttpResponseMessage AprobarIncidencia([FromBody]Models.Asistencia asistencia)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Response<bool> resp = Models.Asistencia.AprobarIncidencia(asistencia);
            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [HttpPost]
        [Route("Web/PenalizarAsistencia")]
        public HttpResponseMessage PenalizarAsistencia([FromBody]Models.Asistencia asistencia)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Response<bool> resp = Models.Asistencia.PenalizarAsistencia(asistencia);
            message.Content = new ObjectContent<Response<bool>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }



    }
}
