﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Web
    /// </summary>
    [RoutePrefix("api/Imagen")]
    public class ImagenController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("{id:int}/File/{esEntrada:int}/")]
        public HttpResponseMessage ObtenerFSImagenID(int id,int esEntrada)
        {
            HttpResponseMessage resp = new HttpResponseMessage();

            Response<FileStream> imagen = Models.Imagen.ObtenerFSImagenID(id,esEntrada);

            resp.Content = new ObjectContent<Response<FileStream>>(imagen, Utiles.Formatter);

            if (imagen.Estatus == Estatus.Exito)
            {
                resp.Content = new StreamContent(imagen.Datos);
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                resp.StatusCode = HttpStatusCode.OK;
            }
            else if (imagen.Estatus == Estatus.Advertencia)
            {
                resp.StatusCode = HttpStatusCode.NotFound;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }

            return resp;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id:int}/File/")]
        public HttpResponseMessage ObtenerImagenPerfil(int id)
        {
            HttpResponseMessage resp = new HttpResponseMessage();

            Response<FileStream> imagen = Models.Imagen.ObtenerImagenPerfil(id);

            resp.Content = new ObjectContent<Response<FileStream>>(imagen, Utiles.Formatter);

            if (imagen.Estatus == Estatus.Exito)
            {
                resp.Content = new StreamContent(imagen.Datos);
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                resp.StatusCode = HttpStatusCode.OK;
            }
            else if (imagen.Estatus == Estatus.Advertencia)
            {
                resp.StatusCode = HttpStatusCode.NotFound;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }

            return resp;
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("Map/Marker/{content}/Visita/SinIniciar")]
        public HttpResponseMessage ObtenerMarcadorVisitaSinIniciar(string content)
        {
            HttpResponseMessage resp = new HttpResponseMessage();

            Response<FileStream> imagen = Models.Imagen.ObtenerMarcadorVisita(content, 1);

            resp.Content = new ObjectContent<Response<FileStream>>(imagen, Utiles.Formatter);

            if (imagen.Estatus == Estatus.Exito)
            {
                resp.Content = new StreamContent(imagen.Datos);
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                resp.StatusCode = HttpStatusCode.OK;
            }
            else if (imagen.Estatus == Estatus.Advertencia)
            {
                resp.StatusCode = HttpStatusCode.NotFound;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }

            return resp;
        }

    }
}
