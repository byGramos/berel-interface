﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelWebServices.Controllers
{
    [RoutePrefix("api/Konesh")]
    public class KoneshController : ApiController
    {
        [HttpGet]
        [Route("test")]
        public string Hello()
        {
            return "Hello!!!";
        }

        [HttpGet]
        [Route("EstatusUUID")]
        public HttpResponseMessage EstatusUUID()
        {

            string uuid = Utiles.GetQueryParamOrNull(Request, "uuid");

            Response<List<Models.Recepcion>> recepcion = Models.Konesh.EstatusUUID(uuid);
            HttpResponseMessage resp = new HttpResponseMessage();

            resp.Content = new ObjectContent<Response<List<Models.Recepcion>>>(recepcion, Utiles.Formatter);

            if (recepcion.Estatus == Estatus.Exito)
            {
                resp.StatusCode = HttpStatusCode.OK;
            }
            else if (recepcion.Estatus == Estatus.Advertencia)
            {
                resp.StatusCode = HttpStatusCode.Conflict;
            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }

            return resp;
        }

    }
}
