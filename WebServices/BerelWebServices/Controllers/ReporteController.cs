﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Web
    /// </summary>
    [RoutePrefix("api/Reportes")]
    public class ReporteController : ApiController
    {

        [HttpGet]
        [Route("test")]
        public string Hello()
        {
            return "Hello!!!";
        }


        [HttpGet]
        [Route("VentaHomeDepot")]
        public HttpResponseMessage VentaHomeDepot()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            
            //Response<List<Models.VentaHD>> resp = Models.VentaHD.ObtenerVentaHD();
            //message.Content = new ObjectContent<Response<List<Models.VentaHD>>>(resp, Utiles.Formatter);

            return Utiles.RespuestaGeneralServicio(() =>Models.VentaHD.GenerarReporteVentaHDExcel());

        }

        [HttpGet]
        [Route("VentaConsolidadaHD")]
        public HttpResponseMessage VentaConsolidadaHD()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            return Utiles.RespuestaGeneralServicio(() => Models.VentaHD.GenerarReporteVentaHDExcel2());

        }

        [HttpGet]
        [Route("UltimaDescarga")]
        public HttpResponseMessage UltimaDescarga()
        {
            HttpResponseMessage message = new HttpResponseMessage();
            
            return Utiles.RespuestaGeneralServicio(() => Models.VentaHD.ObtenerUltimaDescarga());

        }


        [HttpGet]
        [Route("xlsx/{nombre}")]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerReporteExcel(string nombre)
        {
            HttpResponseMessage message = new HttpResponseMessage();
            string fileName = string.Empty;

            fileName = Utiles.GetQueryParamOrDefaultValue(Request, "fileName", "Reporte" + nombre + ".xlsx");

            Response<Stream> resp = Models.Reporte.ObtenerReporteExcel(nombre);

            if (resp.Estatus == Estatus.Exito)
            {
                message.Content = new StreamContent(resp.Datos);
                message.Content.Headers.ContentLength = resp.Datos.Length;
                //message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileName
                };

                message.StatusCode = HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = HttpStatusCode.NotFound;
            }
            else
            {
                message.StatusCode = HttpStatusCode.InternalServerError;
            }

            return message;
        }

 
        [HttpGet]
        [Route("Web/ObtenerReportePromo")]
        public HttpResponseMessage ObtenerReportePromo()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            int? cliente = Utiles.GetQueryParamOrNull<int>(Request, "cliente");

            if (cliente != null)
            {
                Response<List<Models.Promo>> resp = Models.Promo.ObtenerReportePromo(cliente);
                message.Content = new ObjectContent<Response<List<Models.Promo>>>(resp, Utiles.Formatter);
            }
            else
            {
                message.StatusCode = HttpStatusCode.BadRequest;

                message.Content = new ObjectContent<Response<object>>(new Response<object>()
                {
                    Estatus = Estatus.Error,
                    Mensaje = "No se recibio algun campo requerido ",
                    MensajeTecnico = string.Format("cliente={1}", cliente)
                }, Utiles.Formatter);
            }


            return message;
        }

        [HttpGet]
        [Route("Web/ObtenerReportePedidos")]
        public HttpResponseMessage ObtenerReportePedidos()
        {
            HttpResponseMessage message = new HttpResponseMessage();


            Response<List<Models.Promo>> resp = Models.Promo.ObtenerReportePedidos();
            message.Content = new ObjectContent<Response<List<Models.Promo>>>(resp, Utiles.Formatter);
           


            return message;
        }



    }
}
