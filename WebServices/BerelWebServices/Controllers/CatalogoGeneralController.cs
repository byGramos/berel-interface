﻿using BerelInterfaceServices.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BerelInterfaceServices.Controllers
{
    /// <summary>
    /// Metodos para servicio de la aplicacion Web
    /// </summary>
    [RoutePrefix("api/CatalogoGeneral")]
    public class CatalogoGeneralController : ApiController
    {

        [Route("Web/ObtenerSupervisores")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerSupervisores()
        {

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerSupervisores();

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [Route("Web/ObtenerTiposIncidencias")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerTiposIncidencias()
        {

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerTiposIncidencias();

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }


        [Route("Web/ObtenerAgencias")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerAgencias()
        {

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerAgencias();

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [Route("Web/ObtenerPuestos")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerPuestos()
        {

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerPuestos();

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }


        [Route("Web/ObtenerTiendasAsignadas")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerTiendasAsignadas()
        {
            int? id = Utiles.GetQueryParamOrNull<int>(Request, "id");

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerTiendasAsignadas(id);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }

        [Route("Web/ObtenerPromotoresByTienda")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage ObtenerPromotoresByTienda()
        {
            int? idtienda = Utiles.GetQueryParamOrNull<int>(Request, "idTienda");

            Response<List<Models.CatalogoGeneral>> resp = Models.CatalogoGeneral.ObtenerPromotoresByTienda(idtienda);

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new ObjectContent<Response<List<Models.CatalogoGeneral>>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else if (resp.Estatus == Estatus.Error)
            {
                message.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return message;
        }


    }
}
