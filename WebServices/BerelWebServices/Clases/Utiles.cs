﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using System.Linq;
using System.Data;
using System.Net.Http;
using System.Collections.Concurrent;
using System.Globalization;
using System.Net.Http.Headers;
using System.IO;

namespace BerelInterfaceServices.Clases
{
    public class Utiles
    {
        public delegate Response<T> Query<T>();

        public delegate Response<T> Obtener<T>(int id);
        public delegate Response<TReturn> Actualizar<TReturn, TValue>(TValue obj);

        static JsonMediaTypeFormatter formatter = new JsonMediaTypeFormatter();
        public static JsonMediaTypeFormatter Formatter { get { return formatter; } }

        public static CultureInfo FormatDateProvider { get; set; }

        public static Nullable<T> GetQueryParamOrNull<T>(HttpRequestMessage request, string key) where T : struct
        {
            List<KeyValuePair<string, string>> lista = request.GetQueryNameValuePairs().ToList();

            KeyValuePair<string, string> param = lista.FirstOrDefault(p => p.Key == key);

            Nullable<T> resp = null;

            if (param.Value != null)
            {
                resp = (T)Convert.ChangeType(param.Value, typeof(T));
            }

            return resp;
        }

        public static string GetQueryParamOrNull(HttpRequestMessage request, string key)
        {
            List<KeyValuePair<string, string>> lista = request.GetQueryNameValuePairs().ToList();

            KeyValuePair<string, string> param = lista.FirstOrDefault(p => p.Key == key);

            string resp = null;

            if (param.Value != null)
            {
                resp = param.Value;
            }

            return resp;
        }


        public static string MD5(string source)
        {
            StringBuilder builder = new StringBuilder();

            byte[] bytes = ASCIIEncoding.ASCII.GetBytes(source);
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] hash = md5.ComputeHash(bytes);

            for (int i = 0; i < hash.Length; i++)
            {
                builder.Append(hash[i].ToString("X2"));
            }

            return builder.ToString().ToLower();
        }

        public static string GenerarToken()
        {
            StringBuilder builder = new StringBuilder();
            string seed = DateTime.Now.ToString("yyyyMMddHHmmssffff");

            byte[] bytes = ASCIIEncoding.ASCII.GetBytes(seed);
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] hash = md5.ComputeHash(bytes);

            for (int i = 0; i < hash.Length; i++)
            {
                builder.Append(hash[i].ToString("X2"));
            }

            return builder.ToString().ToLower();
        }

        public static string ConvertToJson(object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }

        public static string GuardarImagen(string base64Img)
        {
            if (base64Img != null)
            {
                base64Img = base64Img.Replace("\n", "");

                string hash = Utiles.MD5(base64Img);

                string ruta = getAppConfig("imgPath") + "\\";
                string nombre = getAppConfig("imgPrefix");
                nombre += DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + hash + "_" + ".png";

                string rutaImagen = ruta + nombre;

                byte[] img = Convert.FromBase64String(base64Img);

                System.IO.FileStream imagen = System.IO.File.Open(rutaImagen, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                imagen.Write(img, 0, img.Length);

                imagen.Flush();
                imagen.Close();

                return rutaImagen;
            }
            else
            {
                return "";
            }
        }

        public static string GuardarImagenPerfil(string base64Img)
        {
            if (base64Img != null)
            {
                base64Img = base64Img.Replace("\n", "");

                string hash = Utiles.MD5(base64Img);

                string ruta = getAppConfig("imgPathPerfil") + "\\";
                string nombre = "Promotor_" + getAppConfig("imgPrefix");
                nombre += hash + "_" + ".png";

                string rutaImagen = ruta + nombre;

                byte[] img = Convert.FromBase64String(base64Img);

                System.IO.FileStream imagen = System.IO.File.Open(rutaImagen, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                imagen.Write(img, 0, img.Length);

                imagen.Flush();
                imagen.Close();

                return rutaImagen;
            }
            else
            {
                return "";
            }
        }

        static string getAppConfig(string key)
        {
            string value = "";

            try
            {
                value = System.Configuration.ConfigurationManager.AppSettings.Get(key);

                if (value == null)
                {
                    value = "";
                }
            }
            catch
            {
                value = "";
            }


            return value;
        }

        public static void eliminarImagen(string rutaImagen){

            File.Delete(rutaImagen);

        }


        public static HttpResponseMessage RespuestaGeneralServicio<T>(Query<T> query,
            HttpStatusCode exito = HttpStatusCode.OK,
            HttpStatusCode advertencia = HttpStatusCode.NoContent,
            HttpStatusCode error = HttpStatusCode.InternalServerError
        )
        {
            HttpResponseMessage message = new HttpResponseMessage();

            var resp = query();

            message.Content = new ObjectContent<Response<T>>(resp, Utiles.Formatter);

            if (resp.Estatus == Estatus.Exito)
            {
                message.StatusCode = exito;
            }
            else if (resp.Estatus == Estatus.Advertencia)
            {
                message.StatusCode = advertencia;
            }
            else
            {
                message.StatusCode = error;
            }

            return message;
        }

        public static string GuardarTemporalmenteArchivo(OfficeOpenXml.ExcelPackage pck, string nombreArchivo = null, string extension = null)
        {
            string ruta = "C:\\inetpub\\wwwroot\\";
            nombreArchivo = nombreArchivo ?? Guid.NewGuid().ToString();
            extension = extension ?? ".xlsx";

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            pck.File = new FileInfo(ruta + nombreArchivo + extension);

            pck.Save();

            return nombreArchivo;
        }

        public static T GetQueryParamOrDefaultValue<T>(HttpRequestMessage request, string key, T defValue)
        {
            List<KeyValuePair<string, string>> lista = request.GetQueryNameValuePairs().ToList();

            KeyValuePair<string, string> param = lista.FirstOrDefault(p => p.Key == key);

            T resp = defValue;

            if (param.Value != null)
            {
                resp = (T)Convert.ChangeType(param.Value, typeof(T));
            }

            return resp;
        }

        public static Stream ObtenerYEliminarExcel(string nombreArchivo, string extension = null)
        {
            string ruta = "C:\\inetpub\\wwwroot\\";
            extension = extension ?? ".xlsx";

            if (File.Exists(ruta + nombreArchivo + extension))
            {
                FileStream archivo = new FileStream(ruta + nombreArchivo + extension, FileMode.Open, FileAccess.Read);

                Stream streamFile = new MemoryStream();

                archivo.CopyTo(streamFile);

                archivo.Close();

                File.Delete(ruta + nombreArchivo + extension);

                streamFile.Seek(0, SeekOrigin.Begin);

                return streamFile;
            }
            else
            {
                return null;
            }
        }



    }
}