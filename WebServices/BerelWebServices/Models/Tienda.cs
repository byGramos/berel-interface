﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelInterfaceServices.Models
{
    public class Tienda
    {
        public int ID { get; set; }
        public string Descripcion { get; set; }


        public static Response<List<Tienda>> ObtenerTiendas()
        {
            DataTable data = new BerelWebData.Tienda(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerTiendas();
            Response<List<Tienda>> resp = new Response<List<Tienda>>() { Datos = null, Estatus = Estatus.Error };

            List<Tienda> lista = new List<Tienda>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        Tienda t = new Tienda();

                        t.ID = Convert.ToInt32(row["IDTienda"]);
                        t.Descripcion = row["Tienda"].ToString();

                        lista.Add(t);
                    }                      

                       resp.Datos = lista;
                       resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de tiendas";
                    resp.MensajeTecnico = "No se han registrado Tiendas en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo de tiendas.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<Tienda>> ObtenerTiendasAsignadas(int idUsuario)
        {
            DataTable data = new BerelWebData.Tienda(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerTiendasAsignadas(idUsuario);
            Response<List<Tienda>> resp = new Response<List<Tienda>>() { Datos = null, Estatus = Estatus.Error };

            List<Tienda> lista = new List<Tienda>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        Tienda t = new Tienda();

                        t.ID = Convert.ToInt32(row["IDTienda"]);
                        t.Descripcion = row["Tienda"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de tiendas";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo de tiendas.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }




        public static Response<List<Tienda>> ObtenerTransferencias(int idUsuario)
        {
            DataTable data = new BerelWebData.Tienda(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerTransferencias(idUsuario);
            Response<List<Tienda>> resp = new Response<List<Tienda>>() { Datos = null, Estatus = Estatus.Error };

            List<Tienda> lista = new List<Tienda>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        Tienda t = new Tienda();

                        t.ID = Convert.ToInt32(row["IDTienda"]);
                        t.Descripcion = row["Tienda"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de tiendas";
                    resp.MensajeTecnico = "No se hay registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo de tiendas.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


    }
}