﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;

namespace BerelInterfaceServices.Models
{
    public class VentaHD
    {
        public int ID { get; set; }
        public DateTime FechaDescarga { get; set; }
        public string CodigoTienda { get; set; }
        public string SkuHD { get; set; }
        public string SkuBerel { get; set; }
        public decimal Capacidad { get; set; }
        public string Descripcion { get; set; }
        public string EstatusCod { get; set; }
        public decimal VentaUnidad { get; set; }
        public decimal VentaLitros { get; set; }
        public decimal Existencia { get; set; }
        public decimal ExistenciaLitros { get; set; }
        public int CantidadOrdenada { get; set; }



        public static Response<List<VentaHD>> ObtenerVentaHD()
        {
            DataTable data = new BerelWebData.VentaHD(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerVentaHD();
            Response<List<VentaHD>> resp = new Response<List<VentaHD>>() { Datos = null, Estatus = Estatus.Error };

            List<VentaHD> lista = new List<VentaHD>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        VentaHD u = new VentaHD();

                        u.ID = Convert.ToInt32(row["ID"]);
                        u.FechaDescarga = Convert.ToDateTime(row["FechaDescarga"]);
                        u.CodigoTienda = row["CodigoTienda"].ToString();
                        u.SkuHD = row["SkuHD"].ToString();
                        u.SkuBerel = row["SkuBerel"].ToString();
                        u.Capacidad = Convert.ToDecimal(row["Capacidad"]);
                        u.Descripcion = row["Descripcion"].ToString();
                        u.EstatusCod = row["Estatus"].ToString();
                        u.VentaUnidad = Convert.ToInt32(row["VentaUnidad"]);
                        u.VentaLitros = Convert.ToDecimal(row["VentaLitros"]);
                        u.Existencia = Convert.ToInt32(row["Existencia"]);
                        u.ExistenciaLitros = Convert.ToDecimal(row["ExistenciaLitros"]);
                        u.CantidadOrdenada = Convert.ToInt32(row["CantidadOrdenada"]);

                        lista.Add(u);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de usuarios";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener promotores asignados.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<VentaHD>> ObtenerVentaConsolidadaHD()
        {
            DataTable data = new BerelWebData.VentaHD(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerVentaConsolidadaHD();
            Response<List<VentaHD>> resp = new Response<List<VentaHD>>() { Datos = null, Estatus = Estatus.Error };

            List<VentaHD> lista = new List<VentaHD>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        VentaHD u = new VentaHD();

                        u.FechaDescarga = Convert.ToDateTime(row["FechaDescarga"]);
                        u.CodigoTienda = row["CodigoTienda"].ToString();
                        u.VentaLitros = Convert.ToInt32(row["VentaLitros"]);
                        u.ExistenciaLitros = Convert.ToInt32(row["ExistenciaLitros"]);

                        lista.Add(u);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de usuarios";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener promotores asignados.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<string> ObtenerUltimaDescarga()
        {
            DataTable data = new BerelWebData.VentaHD(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerUltimaDescarga();
            Response<string> resp = new Response<string>();


            try
            {
                if (data.Rows.Count > 0)
                {
                    resp.Datos = data.Rows[0]["Fecha"].ToString() + "|" + data.Rows[0]["Divisor"].ToString();
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de usuarios";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener promotores asignados.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


        public static Response<string> GenerarReporteVentaHDExcel()
        {
            Response<string> resp = new Response<string>();

            try
            {
                var respReporte = ObtenerVentaHD();

                if (respReporte.Estatus == Estatus.Exito)
                {
                    List<VentaHD> reporte = respReporte.Datos;

                    FileStream template = new FileStream(HttpContext.Current.Server.MapPath("~/App_Data/reporte-template/ReporteVentaHD.xlsx"), FileMode.Open, FileAccess.ReadWrite);
                    MemoryStream tempTemplate = new MemoryStream();
                    int dataRowStart = 2;

                    template.CopyTo(tempTemplate);
                    template.Close();

                    OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage(tempTemplate);

                    OfficeOpenXml.ExcelWorksheet sheet = pck.Workbook.Worksheets["SellOut"];

                    sheet.Cells["A1"].Value = "FechaDescarga";
                    sheet.Cells["B1"].Value = "CodigoTienda";
                    sheet.Cells["C1"].Value = "SkuHD";
                    sheet.Cells["D1"].Value = "SkuBerel";
                    sheet.Cells["E1"].Value = "Descripcion";
                    sheet.Cells["F1"].Value = "Estatus";
                    sheet.Cells["G1"].Value = "VentaUnidad";
                    sheet.Cells["H1"].Value = "Existencia"; 
                    sheet.Cells["I1"].Value = "Capacidad";
                    sheet.Cells["J1"].Value = "VentaLitros";
                    sheet.Cells["K1"].Value = "ExistenciaLitros";


                    foreach (VentaHD rep in reporte)
                    {

                        sheet.Cells[dataRowStart, 1].Value = rep.FechaDescarga.ToShortDateString();
                        sheet.Cells[dataRowStart, 2].Value = rep.CodigoTienda;
                        sheet.Cells[dataRowStart, 3].Value = rep.SkuHD;
                        sheet.Cells[dataRowStart, 4].Value = rep.SkuBerel;
                        sheet.Cells[dataRowStart, 5].Value = rep.Descripcion;
                        sheet.Cells[dataRowStart, 6].Value = rep.EstatusCod;
                        sheet.Cells[dataRowStart, 7].Value = rep.VentaUnidad;
                        sheet.Cells[dataRowStart, 8].Value = rep.Existencia;
                        sheet.Cells[dataRowStart, 9].Value = rep.Capacidad;
                        sheet.Cells[dataRowStart, 10].Value = rep.VentaLitros;
                        sheet.Cells[dataRowStart, 11].Value = rep.ExistenciaLitros;

                        if (rep.SkuHD == "513293")
                        {
                            int x = 0;
                        }

                        dataRowStart++;

                    }


                    resp.Datos = Utiles.GuardarTemporalmenteArchivo(pck);
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = respReporte.Estatus;
                    resp.Mensaje = respReporte.Mensaje;
                    resp.MensajeTecnico = respReporte.MensajeTecnico;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo generar el reporte";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<string> GenerarReporteVentaHDExcel2()
        {
            Response<string> resp = new Response<string>();

            try
            {
                var respReporte = ObtenerVentaConsolidadaHD();

                if (respReporte.Estatus == Estatus.Exito)
                {
                    List<VentaHD> reporte = respReporte.Datos;

                    FileStream template = new FileStream(HttpContext.Current.Server.MapPath("~/App_Data/reporte-template/ReporteVentaHDConsol.xlsx"), FileMode.Open, FileAccess.ReadWrite);
                    MemoryStream tempTemplate = new MemoryStream();
                    int dataRowStart = 2;

                    template.CopyTo(tempTemplate);
                    template.Close();

                    OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage(tempTemplate);

                    OfficeOpenXml.ExcelWorksheet sheet = pck.Workbook.Worksheets["SellOut"];

                    sheet.Cells["A1"].Value = "FechaDescarga";
                    sheet.Cells["B1"].Value = "CodigoTienda";
                    sheet.Cells["C1"].Value = "VentaLitros";
                    sheet.Cells["D1"].Value = "ExistenciaLitros";


                    foreach (VentaHD rep in reporte)
                    {

                        sheet.Cells[dataRowStart, 1].Value = rep.FechaDescarga.ToShortDateString();
                        sheet.Cells[dataRowStart, 2].Value = rep.CodigoTienda;
                        sheet.Cells[dataRowStart, 3].Value = rep.VentaLitros;
                        sheet.Cells[dataRowStart, 4].Value = rep.ExistenciaLitros;

                        dataRowStart++;

                    }


                    resp.Datos = Utiles.GuardarTemporalmenteArchivo(pck);
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = respReporte.Estatus;
                    resp.Mensaje = respReporte.Mensaje;
                    resp.MensajeTecnico = respReporte.MensajeTecnico;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo generar el reporte";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

    }
}