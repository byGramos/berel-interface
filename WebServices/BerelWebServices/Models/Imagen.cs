﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace BerelInterfaceServices.Models
{
    public class Imagen
    {
        public int ID { get; set; }
        public int IDRegistro { get; set; }
        public string RutaImagen { get; set; }

        static string repoImagenes = System.Configuration.ConfigurationManager.AppSettings["repoImagenes"];
        static Font fuente = new Font("Verdana", 40, GraphicsUnit.Pixel);
        static Brush brush = new SolidBrush(Color.Red);

        public static Response<Imagen> ObtenerImagenID(int id, int esEntrada)
        {
            Response<Imagen> resp = new Response<Imagen>()
            {
                Estatus = Estatus.Error
            };

            try
            {
                DataTable datos = new BerelWebData.Imagen(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerImagenID(id, esEntrada);

                if (datos.Rows.Count > 0)
                {
                    DataRow row = datos.Rows[0];

                    Imagen img = new Imagen();

                    img.ID = (int)row["ID"];
                    img.RutaImagen = row["RutaImagen"].ToString();

                    resp.Datos = img;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontro la imagen solicitada.";
                    resp.MensajeTecnico = "No se encontro la imagen con el ID dado.";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo obtener la imagen.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<FileStream> ObtenerFSImagenID(int id,int esEntrada)
        {
            Response<Imagen> imagen = ObtenerImagenID(id,esEntrada);
            Response<FileStream> resp = new Response<FileStream>()
            {
                Estatus = Estatus.Error
            };

            if (imagen.Estatus == Estatus.Exito)
            {
                try
                {
                    FileStream file = File.Open(imagen.Datos.RutaImagen, FileMode.Open);
                    resp.Datos = file;
                    resp.Estatus = Estatus.Exito;
                }
                catch (Exception ex)
                {
                    resp.Mensaje = "No se pudo obtener la imagen";
                    resp.MensajeTecnico = ex.Message;
                }
            }
            else
            {
                resp.Estatus = imagen.Estatus;
                resp.Mensaje = imagen.Mensaje;
                resp.MensajeTecnico = imagen.MensajeTecnico;
            }

            return resp;
        }

        public static Response<FileStream> ObtenerMarcadorVisita(string contenido, int estatusVisita)
        {
            Response<FileStream> resp = new Response<FileStream>()
            {
                Estatus = Estatus.Advertencia
            };

            try
            {
                FileStream fsImagen = null;

                if (estatusVisita == 1)//Visita sin iniciar
                {
                    if (File.Exists(repoImagenes + "info_marker.png"))
                    {
                        fsImagen = new FileStream(repoImagenes + "info_marker.png", FileMode.Open);
                    }
                }
                else if (estatusVisita == 2)//Visita en Proceso
                {
                    if (File.Exists(repoImagenes + "yellow_marker.png"))
                    {
                        fsImagen = new FileStream(repoImagenes + "yellow_marker.png", FileMode.Open);
                    }
                }
                else if (estatusVisita == 3)//Visita terminada
                {
                    if (File.Exists(repoImagenes + "green_marker.png"))
                    {
                        fsImagen = new FileStream(repoImagenes + "green_marker.png", FileMode.Open);
                    }
                }

                Bitmap imagenOriginal = new Bitmap(fsImagen);

                Graphics grap = Graphics.FromImage(imagenOriginal);
                grap.DrawString(contenido, fuente, brush, 15, 9);

                grap.Save();
                imagenOriginal.Save(resp.Datos, System.Drawing.Imaging.ImageFormat.Png);

                resp.Estatus = Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = ex.Message;
            }

            return resp;
        }

        public static Response<FileStream> ObtenerImagenPerfil(int id)
        {
            Response<Imagen> imagen = ObtenerImagenPerfilID(id);
            Response<FileStream> resp = new Response<FileStream>()
            {
                Estatus = Estatus.Error
            };

            if (imagen.Estatus == Estatus.Exito)
            {
                try
                {
                    FileStream file = File.Open(imagen.Datos.RutaImagen, FileMode.Open);
                    resp.Datos = file;
                    resp.Estatus = Estatus.Exito;
                }
                catch (Exception ex)
                {
                    resp.Mensaje = "No se pudo obtener la imagen";
                    resp.MensajeTecnico = ex.Message;
                }
            }
            else
            {
                resp.Estatus = imagen.Estatus;
                resp.Mensaje = imagen.Mensaje;
                resp.MensajeTecnico = imagen.MensajeTecnico;
            }

            return resp;
        }

        public static Response<Imagen> ObtenerImagenPerfilID(int id)
        {
            Response<Imagen> resp = new Response<Imagen>()
            {
                Estatus = Estatus.Error
            };

            try
            {
                DataTable datos = new BerelWebData.Imagen(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerImagenPerfil(id);

                if (datos.Rows.Count > 0)
                {
                    DataRow row = datos.Rows[0];

                    Imagen img = new Imagen();

                    img.ID = (int)row["ID"];
                    img.RutaImagen = row["RutaImagen"].ToString();

                    resp.Datos = img;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontro la imagen solicitada.";
                    resp.MensajeTecnico = "No se encontro la imagen con el ID dado.";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo obtener la imagen.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


    }
}