﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelInterfaceServices.Models
{
    public class Asistencia
    {
        public int IDVisita { get; set; }
        public int IDUsuario { get; set; }
        public string Promotor { get; set; }
        public string Tienda { get; set; }
        public DateTime FechaVisita { get; set; }
        public DateTime HoraEntrada { get; set; }
        public string GPSEntrada { get; set; }
        public DateTime HoraInicioComida { get; set; }
        public DateTime HoraFinComida { get; set; }
        public DateTime HoraSalida { get; set; }
        public string GPSSalida { get; set; }
        public string ImagenEntrada { get; set; }
        public string ImagenSalida { get; set; }
        public string Estatus { get; set; }
        public string Comentario { get; set; }
        public int UsuarioReg { get; set; }
        public int TiempoTotal { get; set; }


        public static Response<List<Asistencia>> ObtenerAsistencia(DateTime fechaInicio, DateTime fechaFin)
        {
            Response<List<Asistencia>> resp = new Response<List<Asistencia>>()
            {
                Estatus = Clases.Estatus.Error
            };

            List<Asistencia> lista = new List<Asistencia>();

            try
            {
                DataTable datos = new BerelWebData.Asistencia(new BerelWebData.ConexionBD("SQL")).ObtenerAsistencia(fechaInicio, fechaFin);

                if (datos.Rows.Count > 0)
                {
                    foreach (DataRow row in datos.Rows)
                    {
                        Asistencia p = new Asistencia();

                        p.IDVisita = (int)row["IDVisita"];
                        p.IDUsuario = (int)row["IDUsuario"];
                        p.Promotor = row["Promotor"].ToString();
                        p.Tienda = row["Tienda"].ToString();
                        p.FechaVisita = (DateTime)row["FechaVisita"];
                        p.HoraEntrada = (DateTime)row["HoraEntrada"];
                        p.GPSEntrada = row["GPSEntrada"].ToString();
                        p.HoraInicioComida = (DateTime)row["HoraInicioComida"];
                        p.HoraFinComida = (DateTime)row["HoraFinComida"];
                        p.HoraSalida = (DateTime)row["HoraSalida"];
                        p.GPSSalida = row["GPSSalida"].ToString();
                        p.ImagenEntrada = row["ImagenEntrada"].ToString();
                        p.ImagenSalida = row["ImagenSalida"].ToString();
                        p.Estatus = row["Estatus"].ToString();
                        p.TiempoTotal = (int)row["TotalMinutos"];

                        lista.Add(p);
                    }

                    resp.Estatus = Clases.Estatus.Exito;
                    resp.Datos = lista;
                }
                else
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudieron obtener los registros";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


        public static Response<bool> ReportarIncidencia(Asistencia asistencia)
        {
            Response<bool> resp = new Response<bool>()
            {
                Estatus = Clases.Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var AsistenciaData = new BerelWebData.Asistencia(conexion);

            try
            {
                conexion.BeginTransaction();

                //int rowAfectado = IncidenciaData.ClasificarIncidencia(idIncidencia,tipoIncidencia, usuario);
                int rowAfectado = AsistenciaData.ReportarIncidencia(asistencia.IDVisita, asistencia.Comentario, asistencia.UsuarioReg);

                conexion.Commit();

                if (rowAfectado == 0)
                {
                    resp.Estatus = Clases.Estatus.Error;
                    resp.Mensaje = "No se pudo guardar el registro";
                }
                else if (rowAfectado == -1)
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                    resp.Mensaje = "El reporte para esta visita ya fue registrado";
                }
                else
                    resp.Estatus = Clases.Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo realizar la transaccion \n Contacte a Sistemas";
                resp.MensajeTecnico = ex.Message;
                conexion.Rollback();
            }

            return resp;
        }


        public static Response<bool> AprobarIncidencia(Asistencia asistencia)
        {
            Response<bool> resp = new Response<bool>()
            {
                Estatus = Clases.Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var AsistenciaData = new BerelWebData.Asistencia(conexion);

            try
            {
                conexion.BeginTransaction();

                //int rowAfectado = IncidenciaData.ClasificarIncidencia(idIncidencia,tipoIncidencia, usuario);
                int rowAfectado = AsistenciaData.AprobarIncidencia(asistencia.IDVisita, asistencia.Comentario, asistencia.UsuarioReg);

                conexion.Commit();

                if (rowAfectado == 0)
                {
                    resp.Estatus = Clases.Estatus.Error;
                    resp.Mensaje = "No se pudo guardar el registro";
                }
                else if (rowAfectado == -1)
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                    resp.Mensaje = "El reporte para esta visita ya fue registrado";
                }
                else
                    resp.Estatus = Clases.Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo realizar la transaccion \n Contacte a Sistemas";
                resp.MensajeTecnico = ex.Message;
                conexion.Rollback();
            }

            return resp;
        }


        public static Response<bool> PenalizarAsistencia(Asistencia asistencia)
        {
            Response<bool> resp = new Response<bool>()
            {
                Estatus = Clases.Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var AsistenciaData = new BerelWebData.Asistencia(conexion);

            try
            {
                conexion.BeginTransaction();

                int rowAfectado = AsistenciaData.PenalizarAsistencia(asistencia.IDVisita, asistencia.UsuarioReg);

                conexion.Commit();

                if (rowAfectado == 0)
                {
                    resp.Estatus = Clases.Estatus.Error;
                    resp.Mensaje = "No se pudo guardar el registro";
                }
                else if (rowAfectado == -1)
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                    resp.Mensaje = "El ya fue registrada esta visita";
                }
                else
                    resp.Estatus = Clases.Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo realizar la transaccion \n Contacte a Sistemas";
                resp.MensajeTecnico = ex.Message;
                conexion.Rollback();
            }

            return resp;
        }        


    }
}