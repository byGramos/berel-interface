﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelInterfaceServices.Models
{
    public class Promo
    {
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Linea { get; set; }
        public string Tope { get; set; }
        public string Litros { get; set; }
        public string Estatus { get; set; }
        public string Disponible { get; set; }
        public string Pedido { get; set; }
        public DateTime Fecha { get; set; }
        public string Ultimo { get; set; }
        public string Siguiente { get; set; }
        public string Peso { get; set; }



        public static Response<List<Promo>> ObtenerReportePromo(int? cliente)
        {
            Response<List<Promo>> resp = new Response<List<Promo>>()
            {
                Estatus = Clases.Estatus.Error
            };

            List<Promo> lista = new List<Promo>();

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var PromoData = new BerelWebData.Promo(conexion);
            
            try
            {
                DataTable datos = new BerelWebData.Promo(new BerelWebData.ConexionBD("SQL")).ObtenerReportePromo(cliente);

                if (datos.Rows.Count > 0)
                {
                    foreach (DataRow row in datos.Rows)
                    {
                        Promo u = new Promo();

                        u.Cliente = row["Cliente"].ToString();
                        u.Nombre = row["Nombre"].ToString();
                        u.Linea = row["Linea"].ToString();
                        u.Tope = row["Tope"].ToString();
                        u.Litros = row["Litros"].ToString();
                        u.Estatus = row["Estatus"].ToString();
                        u.Disponible = row["Disponible"].ToString();

                        lista.Add(u);

                    }

                    resp.Estatus = Clases.Estatus.Exito;
                    resp.Datos = lista;
                }
                else
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudieron obtener los registros";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<Promo>> ObtenerReportePedidos()
        {
            Response<List<Promo>> resp = new Response<List<Promo>>()
            {
                Estatus = Clases.Estatus.Error
            };

            List<Promo> lista = new List<Promo>();

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var PromoData = new BerelWebData.Promo(conexion);

            try
            {
                DataTable datos = new BerelWebData.Promo(new BerelWebData.ConexionBD("SQL")).ObtenerReportePedidos();

                if (datos.Rows.Count > 0)
                {
                    foreach (DataRow row in datos.Rows)
                    {
                        Promo u = new Promo();

                        u.Pedido = row["Pedido"].ToString();
                        u.Fecha = (DateTime)row["Fecha"];
                        u.Cliente = row["Cliente"].ToString();
                        u.Nombre = row["Nombre"].ToString();
                        u.Ultimo = row["Ultimo"].ToString();
                        u.Siguiente = row["Siguiente"].ToString();
                        u.Peso = row["Peso"].ToString();
                        
                        lista.Add(u);

                    }

                    resp.Estatus = Clases.Estatus.Exito;
                    resp.Datos = lista;
                }
                else
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudieron obtener los registros";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


    }
}