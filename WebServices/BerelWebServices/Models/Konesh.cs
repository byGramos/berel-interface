﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelWebServices.Models
{
    public class Recepcion
    {
        public string Serie { get; set; }
        public string Folio { get; set; }
        public string RfcEmisor { get; set; }
        public string NombreEmisor { get; set; }
        public decimal Total { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public string Uuid { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

    }

    public class Konesh
    {

        public static Response<List<Recepcion>> EstatusUUID(string uuid)
        {
            DataTable data = new BerelWebData.Konesh(new BerelWebData.ConexionBD("cfdportalDB")).EstatusUUID(uuid);
            Response<List<Recepcion>> resp = new Response<List<Recepcion>>() { Datos = null, Estatus = Estatus.Error };

            if (data.Rows.Count == 0)
            {
                data = new BerelWebData.Konesh(new BerelWebData.ConexionBD("cfdportalDB")).EstatusUUIDErr(uuid);
            }

            List<Recepcion> lista = new List<Recepcion>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        Recepcion u = new Recepcion();


                        u.Serie = row["SERIE"].ToString();
                        u.Folio = row["FOLIO"].ToString();
                        u.RfcEmisor = row["RFCEMISOR"].ToString();
                        u.NombreEmisor = row["NOMBREEMISOR"].ToString();
                        u.FechaEmision = Convert.ToDateTime(row["FECHA_EMISION"]);
                        u.FechaRecepcion = Convert.ToDateTime(row["FECHA_RECEPCION"]);                        
                        u.Total = Convert.ToDecimal(row["TOTAL"]);
                        u.Uuid = row["UUID"].ToString();
                        u.Descripcion = row["DESCRIPCION"].ToString();
                        u.Estado = row["ESTADO"].ToString();

                        lista.Add(u);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Exito;
                    resp.Mensaje = "No se encontraron registros para este uuid";
                    resp.MensajeTecnico = "No existen registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener el UUID consultado";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


    }
}