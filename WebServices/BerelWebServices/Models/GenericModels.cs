﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BerelInterfaceServices.Models
{
    public class Login
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string VersionApp { get; set; }
    }
}