﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;


namespace BerelInterfaceServices.Models
{
    public class Reporte
    {
        public static Response<Stream> ObtenerReporteExcel(string nombre)
        {
            Response<Stream> resp = new Response<Stream>();

            try
            {
                resp.Datos = Utiles.ObtenerYEliminarExcel(nombre);

                if (resp.Datos == null)
                {
                    resp.Estatus = Estatus.Advertencia;
                }
                else
                {
                    resp.Estatus = Estatus.Exito;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo obtener el reporte.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }
    }
}