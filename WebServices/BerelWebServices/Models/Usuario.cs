﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelInterfaceServices.Models
{
    public class Usuario
    {
        public int ID { get; set; }
        public int Nomina { get; set; }
        public string Nombre { get; set; }
        public int IDAgencia { get; set; }
        public string Agencia { get; set; }
        public decimal Sueldo { get; set; }
        public int IDPuesto { get; set; }
        public string Puesto { get; set; }
        public int IDSupervisor { get; set; }
        public string Supervisor { get; set; }
        public int IDTienda { get; set; }
        public string CodigoHD { get; set; }
        public string Tienda { get; set; }
        public string Codigo { get; set; }
        public string Password { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaBaja { get; set; }
        public string User { get; set; }
        public string VersionSistema { get; set; }
        public bool? PermiteDesbloqueoSL { get; set; }
        public string CodigoSureLock { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string Turno { get; set; }
        public int EsMedioTiempo { get; set; }
        public string Imagen { get; set; }
        public string SoporteTecnico { get; set; }
        public int EstatusPromotor { get; set; }
        public bool? Activo { get; set; }


        public static Response<Usuario> iniciarSesion(string usuario,string password,string versionApp)
        {
            Clases.Response<Usuario> resp = new Clases.Response<Usuario>()
            {
                Datos = null,
                Estatus = Clases.Estatus.Advertencia
            };

            // Validamos el codigo ingresado por el promotor
            if (usuario == null || usuario.Length == 0)
            {
                resp.Mensaje = "El usuario debe de tener al menos 1 caracter";
                resp.MensajeTecnico = "El usuario debe de tener al menos 1 caracter";
                return resp;
            }

            try
            {
                string token = Clases.Utiles.GenerarToken();
                Response<Usuario> persona = ObtenerUsuarioLogin(usuario,password,versionApp);

                resp.Estatus = persona.Estatus; // Estatus.Exito;
                resp.Datos = persona.Datos;
                resp.Token = persona.Token;

                resp.Mensaje = persona.Mensaje;
                resp.MensajeTecnico = persona.MensajeTecnico;


            }
            catch(Exception ex)
            {
                resp.Mensaje = "Ocurrio un error al tratar de iniciar sesion, Vuelva a intentar.";
                resp.MensajeTecnico = ex.Message;
                resp.Estatus = Estatus.Error;
            }

            return resp;
        }

        
        public static Response<Usuario> ObtenerUsuarioLogin(string user,string password,string versionApp)
        {
            DataTable data = new BerelWebData.Usuario(new BerelWebData.ConexionBD("SQL")).ObtenerUsuarioLogin(user, password, versionApp);
            Response<Usuario> resp = new Response<Usuario>() { Datos = null, Estatus = Estatus.Error };

            try
            {
                if (data.Rows.Count > 0 )
                {
                    DataRow row = data.Rows[0];
                    Usuario u = new Usuario();

                    if (Convert.ToInt32(row["ID"]) == -1)
                    {
                        resp.Estatus = Estatus.Advertencia;
                        resp.Mensaje = "Usuario inactivo en sistema";
                        resp.MensajeTecnico = "El usuario esta inactivo en DB.";
                    }
                    else if (Convert.ToInt32(row["ID"]) == -2)
                    {
                        resp.Estatus = Estatus.Advertencia;
                        resp.Mensaje = "Código ó contraseña invalido, vuelva a intentarlo.";
                        resp.MensajeTecnico = "El usuario con el password dado no corresponde a algun registro.";
                    }
                    else
                    {
                        u.ID = Convert.ToInt32(row["ID"]);
                        u.IDPuesto = Convert.ToInt32(row["IDPuesto"]);
                        u.Nombre = row["Nombre"].ToString();
                        u.Codigo = row["Codigo"] == DBNull.Value ? null : row["Codigo"].ToString();
                        u.VersionSistema = row["VersionSistema"].ToString();
                        u.FechaActualizacion =  Convert.ToDateTime(row["FechaActualizacion"]);
                        u.Activo = Convert.ToBoolean(row["Activo"]);
                        u.PermiteDesbloqueoSL = Convert.ToBoolean(row["PermiteDesbloqueoSL"]);
                        u.CodigoSureLock = row["CodigoSureLock"].ToString();
                        u.SoporteTecnico = row["SoporteTecnico"].ToString();

                        resp.Datos = u;
                        resp.Estatus = Estatus.Exito;

                    }

                    
                }
                else
                {

                    resp.Estatus = Estatus.Error;
                    resp.Mensaje = "No se pudo obtener el usuario";
                    resp.MensajeTecnico = "El usuario con el password dado no corresponde a algun registro.";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema con la transaccion. Vuelva a intentarlo";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<Usuario>> ObtenerPromotores(int idUsuario)
        {
            DataTable data = new BerelWebData.Usuario(new BerelWebData.ConexionBD("SQL")).ObtenerPromotores(idUsuario);
            Response<List<Usuario>> resp = new Response<List<Usuario>>() { Datos = null, Estatus = Estatus.Error };

            List<Usuario> lista = new List<Usuario>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        Usuario u = new Usuario();

                        u.ID = Convert.ToInt32(row["IDUsuario"]);
                        u.Nombre = row["Nombre"].ToString();

                        lista.Add(u);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de usuarios";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener promotores asignados.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<bool> GuardarUsuario(Usuario usuario)
        {
            Response<bool> resp = new Response<bool>()
            {
                Estatus = Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var UsuarioData = new BerelWebData.Usuario(conexion);
            string rutaImagen = "NaN";

            try
            {
                conexion.BeginTransaction();

                if (usuario.Imagen != null)
                {
                    rutaImagen = Utiles.GuardarImagenPerfil(usuario.Imagen);
                }

                int rowAfectado = UsuarioData.GuardarUsuario(usuario.Nomina, usuario.Nombre, usuario.IDAgencia, usuario.Sueldo, usuario.IDPuesto, usuario.IDSupervisor,usuario.IDTienda, usuario.Codigo,usuario.Password, usuario.FechaIngreso,usuario.User,rutaImagen,usuario.EsMedioTiempo);

                conexion.Commit();

                if (rowAfectado == 0)
                {
                    resp.Estatus = Estatus.Error;
                    resp.Mensaje = "No se pudo Guardar el usuario";
                }
                else if (rowAfectado == -1)
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "Usuario ya fue registrado";
                }
                else
                    resp.Estatus = Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo realizar la accion \n Contacte a Sistemas";
                resp.MensajeTecnico = ex.Message;
                conexion.Rollback();
            }

            return resp;
        }

        public static Response<List<Usuario>> ObtenerUsuarios(DateTime fechaInicio, DateTime fechaFin,int? idSupervisor,int? idAgencia,int? estatus)
        {
            Response<List<Usuario>> resp = new Response<List<Usuario>>()
            {
                Estatus = Clases.Estatus.Error
            };

            List<Usuario> lista = new List<Usuario>();

            try
            {
                DataTable datos = new BerelWebData.Usuario(new BerelWebData.ConexionBD("SQL")).ObtenerUsuarios(fechaInicio, fechaFin, idSupervisor, idAgencia, estatus);

                if (datos.Rows.Count > 0)
                {
                    foreach (DataRow row in datos.Rows)
                    {
                        Usuario u = new Usuario();

                        u.ID = (int)row["ID"];
                        u.Nomina = (int)row["Nomina"];
                        u.Nombre = row["Nombre"].ToString();
                        u.Agencia = row["Agencia"].ToString();
                        u.Sueldo = (Decimal)row["Sueldo"];
                        u.Codigo = row["Codigo"].ToString();
                        u.Puesto = row["Puesto"].ToString();
                        u.Supervisor = row["Supervisor"].ToString();
                        u.CodigoHD = row["CodigoHD"].ToString();
                        u.Tienda = row["Tienda"].ToString();
                        u.EsMedioTiempo = (int)row["EsMedioTiempo"];
                        u.Turno = row["Turno"].ToString();
                        u.FechaIngreso = (DateTime)row["Fechaingreso"];
                        u.EstatusPromotor = (int)row["Estatus"];
                        u.Activo = (bool)row["Activo"];


                        lista.Add(u);
                    }

                    resp.Estatus = Clases.Estatus.Exito;
                    resp.Datos = lista;
                }
                else
                {
                    resp.Estatus = Clases.Estatus.Advertencia;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudieron obtener los registros";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


        public static Response<Usuario> ObtenerUsuario(int? idUsuario)
        {
            Response<Usuario> resp = new Response<Usuario>()
            {
                Estatus = Clases.Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var UsuarioData = new BerelWebData.Usuario(conexion);

            Usuario u = new Usuario();

            try
            {
                DataTable datos = new BerelWebData.Usuario(new BerelWebData.ConexionBD("SQL")).ObtenerUsuario(idUsuario);

                if (datos.Rows.Count > 0)
                    {
                        foreach (DataRow row in datos.Rows)
                        {
                            

                            u.ID = (int)row["ID"];
                            u.Nomina = (int)row["Nomina"];
                            u.Nombre = row["Nombre"].ToString();
                            u.IDAgencia = (int)row["IDAgencia"];
                            u.Agencia = row["Agencia"].ToString();
                            u.Sueldo = (Decimal)row["Sueldo"];
                            u.Codigo = row["Codigo"].ToString();
                            u.Password = row["Password"].ToString();
                            u.IDPuesto = (int)row["IDPuesto"];
                            u.Puesto = row["Puesto"].ToString();
                            u.IDSupervisor = (int)row["IDSupervisor"];
                            u.Supervisor = row["Supervisor"].ToString();
                            u.IDTienda = (int)row["IDTienda"];                            
                            u.Tienda = row["Tienda"].ToString();
                            u.EsMedioTiempo = (int)row["EsMedioTiempo"];
                            u.Turno = row["Turno"].ToString();
                            u.FechaIngreso = (DateTime)row["Fechaingreso"];
                            u.FechaBaja = (DateTime)row["FechaBaja"];
                            u.Imagen = row["ImagenPerfil"].ToString();
                            u.EstatusPromotor = (int)row["Estatus"];
                            u.Activo = (bool)row["Activo"];


                        }

                        resp.Estatus = Clases.Estatus.Exito;
                        resp.Datos = u;
                    }
                    else
                    {
                        resp.Estatus = Clases.Estatus.Advertencia;
                    }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudieron obtener los registros";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


        public static Response<bool> ActualizarUsuario(Usuario usuario)
        {
            Response<bool> resp = new Response<bool>()
            {
                Estatus = Estatus.Error
            };

            BerelWebData.ConexionBD conexion = new BerelWebData.ConexionBD("SQL");
            var UsuarioData = new BerelWebData.Usuario(conexion);
            string rutaImagen = "NaN";

            try
            {
                conexion.BeginTransaction();

                if (usuario.Imagen != null)
                {
                    rutaImagen = Utiles.GuardarImagenPerfil(usuario.Imagen);
                }

                int rowAfectado = UsuarioData.ActualizarUsuario(usuario.ID, usuario.Nomina, usuario.Nombre, usuario.IDAgencia, usuario.Sueldo, usuario.IDPuesto, usuario.IDSupervisor, usuario.IDTienda, usuario.Codigo, usuario.Password, usuario.User,rutaImagen,usuario.EsMedioTiempo);

                conexion.Commit();

                if (rowAfectado == 0)
                {
                    resp.Estatus = Estatus.Error;
                    resp.Mensaje = "No se pudo Guardar el usuario";
                }
                else if (rowAfectado == -1)
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "Usuario ya fue registrado";
                }
                else
                    resp.Estatus = Estatus.Exito;

            }
            catch (Exception ex)
            {
                resp.Mensaje = "No se pudo realizar la accion \n Contacte a Sistemas";
                resp.MensajeTecnico = ex.Message;
                conexion.Rollback();
            }

            return resp;
        }


        public static Response<bool> ActivarUsuario(int? idusuario, int? estatusPromotor,DateTime fechaBaja)
        {

            Response<bool> resp = new Response<bool>()
            {
                Estatus = Estatus.Error
            };

            try
            {
                bool activado = new BerelWebData.Usuario(new BerelWebData.ConexionBD("SQL")).ActivarUsuario(idusuario, estatusPromotor, fechaBaja);

                if (activado)
                {
                    resp.Estatus = Estatus.Exito;
                    resp.Datos = activado;
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Se produjo un error al actualizar el estatus del Usuario";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }




        
    }
}