﻿using BerelInterfaceServices.Clases;
using BerelWebData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BerelInterfaceServices.Models
{
    public class CatalogoGeneral
    {
        public int ID { get; set; }
        public string Descripcion { get; set; }

        public static Response<List<CatalogoGeneral>> ObtenerSupervisores()
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerSupervisores();
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral t = new CatalogoGeneral();

                        t.ID = Convert.ToInt32(row["IDSupervisor"]);
                        t.Descripcion = row["Descripcion"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron Registros";
                    resp.MensajeTecnico = "No se hay registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<CatalogoGeneral>> ObtenerTiposIncidencias()
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerTiposIncidencias();
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral t = new CatalogoGeneral();

                        t.ID = Convert.ToInt32(row["IDTipoIncidencia"]);
                        t.Descripcion = row["Descripcion"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron Registros";
                    resp.MensajeTecnico = "No se hay registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }


        public static Response<List<CatalogoGeneral>> ObtenerAgencias()
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerAgencias();
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral t = new CatalogoGeneral();

                        t.ID = Convert.ToInt32(row["IDAgencia"]);
                        t.Descripcion = row["Descripcion"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron Registros";
                    resp.MensajeTecnico = "No se hay registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<CatalogoGeneral>> ObtenerPuestos()
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerPuestos();
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral t = new CatalogoGeneral();

                        t.ID = Convert.ToInt32(row["IDPuesto"]);
                        t.Descripcion = row["Descripcion"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron Registros";
                    resp.MensajeTecnico = "No se hay registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<CatalogoGeneral>> ObtenerTiendasAsignadas(int? idUsuario)
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerTiendasAsignadas(idUsuario);
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral t = new CatalogoGeneral();

                        t.ID = Convert.ToInt32(row["IDTienda"]);
                        t.Descripcion = row["Tienda"].ToString();

                        lista.Add(t);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de tiendas";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener catalogo de tiendas.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }

        public static Response<List<CatalogoGeneral>> ObtenerPromotoresByTienda(int? idTienda)
        {
            DataTable data = new BerelWebData.CatalogoGeneral(new BerelWebData.ConexionBD("berelRetailDB")).ObtenerPromotoresByTienda(idTienda);
            Response<List<CatalogoGeneral>> resp = new Response<List<CatalogoGeneral>>() { Datos = null, Estatus = Estatus.Error };

            List<CatalogoGeneral> lista = new List<CatalogoGeneral>();

            try
            {
                if (data.Rows.Count > 0)
                {

                    foreach (DataRow row in data.Rows)
                    {
                        CatalogoGeneral u = new CatalogoGeneral();

                        u.ID = Convert.ToInt32(row["IDUsuario"]);
                        u.Descripcion = row["Nombre"].ToString();

                        lista.Add(u);
                    }

                    resp.Datos = lista;
                    resp.Estatus = Estatus.Exito;
                }
                else
                {
                    resp.Estatus = Estatus.Advertencia;
                    resp.Mensaje = "No se encontraron registros de usuarios";
                    resp.MensajeTecnico = "No se han registrado registros en la base de datos";
                }
            }
            catch (Exception ex)
            {
                resp.Mensaje = "Ocurrio un problema al obtener promotores asignados.";
                resp.MensajeTecnico = ex.Message;
            }

            return resp;
        }




    }
}