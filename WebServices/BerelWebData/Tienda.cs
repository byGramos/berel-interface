﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;


namespace BerelWebData
{
    public class Tienda
    {
        public ConexionBD conexion;

        public Tienda(ConexionBD con)
        {
            conexion = con;
        }


        public DataTable ObtenerTiendas()
        {
            if(conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerTiendas");
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerTiendas");
            }
            
        }

        public DataTable ObtenerTransferencias(int idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerTransferencias", new SqlParameter[]
                {
                    new SqlParameter("@IDUsuario", SqlDbType.Int) {Value = idUsuario}
                });
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerTransferencias", new MySqlParameter[]
                {
                    new MySqlParameter("@IDUsuario", MySqlDbType.Int32) {Value = idUsuario}
                });
            }
            
        }


        public DataTable ObtenerTiendasAsignadas(int idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerTiendasAsignadas", new SqlParameter[]
                {
                    new SqlParameter("@IDUsuario", SqlDbType.Int) {Value = idUsuario}
                });
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerTiendasAsignadas", new MySqlParameter[]
                {
                    new MySqlParameter("@IDUsuario", MySqlDbType.Int32) {Value = idUsuario}
                });
            }
            
        }

        public DataTable ObtenerEstatusTienda(int idTienda,int idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerEstatusTienda", new SqlParameter[]
                {
                    new SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda },
                    new SqlParameter("@IDUsuario", SqlDbType.Int) { Value = idUsuario }
                });
            }
            else 
            {
                return conexion.GenericSPSelectMysql("ObtenerEstatusTienda", new MySqlParameter[]
                {
                    new MySqlParameter("@IDTienda_", MySqlDbType.Int32) { Value = idTienda },
                    new MySqlParameter("@IDUsuario_", MySqlDbType.Int32) { Value = idUsuario }
                });
            }
           
        }


    }
}
