﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Asistencia
    {
        public ConexionBD conexion;

        public Asistencia(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable ObtenerAsistencia(DateTime fechaInicio, DateTime fechaFin)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerAsistencia", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerAsistencia", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio",MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin}
                });
            }
            
        }

        public int ReportarIncidencia(int? idVisita, string comentario, int? usuarioReg)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("GuardarIncidenciaParcial", new SqlParameter[]
                {
                    new SqlParameter("@IDVisita", SqlDbType.Int) {Value = idVisita},
                    new SqlParameter("@Comentario", SqlDbType.VarChar) {Value = comentario},
                    new SqlParameter("@IDUsuarioReg", SqlDbType.Int) {Value = usuarioReg}

                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("GuardarIncidenciaParcial", new MySqlParameter[]
                {
                    new MySqlParameter("@IDVisita", MySqlDbType.Int32) {Value = idVisita},
                    new MySqlParameter("@Comentario", MySqlDbType.VarChar) {Value = comentario},
                    new MySqlParameter("@IDUsuarioReg", MySqlDbType.Int32) {Value = usuarioReg}

                });
            }

        }

        public int AprobarIncidencia(int? idVisita, string comentario, int? usuarioReg)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("AprobarIncidenciaParcial", new SqlParameter[]
                {
                    new SqlParameter("@IDVisita", SqlDbType.Int) {Value = idVisita},
                    new SqlParameter("@Comentario", SqlDbType.VarChar) {Value = comentario},
                    new SqlParameter("@IDUsuarioReg", SqlDbType.Int) {Value = usuarioReg}

                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("AprobarIncidenciaParcial", new MySqlParameter[]
                {
                    new MySqlParameter("@IDVisita", MySqlDbType.Int32) {Value = idVisita},
                    new MySqlParameter("@Comentario", MySqlDbType.VarChar) {Value = comentario},
                    new MySqlParameter("@IDUsuarioReg", MySqlDbType.Int32) {Value = usuarioReg}

                });
            }

        }

        public int PenalizarAsistencia(int? idVisita, int? usuarioReg)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("PenalizarAsistencia", new SqlParameter[]
                {
                    new SqlParameter("@IDVisita", SqlDbType.Int) {Value = idVisita},
                    new SqlParameter("@IDUsuarioReg", SqlDbType.Int) {Value = usuarioReg}

                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("PenalizarAsistencia", new MySqlParameter[]
                {
                    new MySqlParameter("@IDVisita", MySqlDbType.Int32) {Value = idVisita},
                    new MySqlParameter("@IDUsuarioReg", MySqlDbType.Int32) {Value = usuarioReg}

                });
            }

        }


    }
}
