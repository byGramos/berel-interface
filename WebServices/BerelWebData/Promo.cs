﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Promo
    {
        public ConexionBD conexion;

        public Promo(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable ObtenerReportePromo(int? cliente)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerTopePromo", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@Cliente", SqlDbType.Int) { Value = cliente}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerReportePromo", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@Cliente", MySqlDbType.Int32) { Value = cliente}
                });
            }

        }

        public DataTable ObtenerReportePedidos()
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerReportePedidos", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.Int) { Value = 1}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerReportePedidos", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@Cliente", MySqlDbType.Int32) { Value = 1}
                });
            }

        }

    }
}
