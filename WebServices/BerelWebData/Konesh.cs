﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Konesh
    {
         public ConexionBD conexion;

         public Konesh(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable EstatusUUID(string uuid)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("SELECT SERIE,FOLIO,RFCEMISOR,NOMBREEMISOR,TOTAL,FECHA_EMISION,FECHA_RECEPCION,UUID,'ACEPTADO' ESTADO,'' DESCRIPCION FROM cfdportal.CFD_RECEPCION WHERE UUID = @uuid", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@uuid", SqlDbType.VarChar) { Value = uuid}
                });
            }
            else
            {
                return this.conexion.GenericSelectMysql("SELECT SERIE,FOLIO,RFCEMISOR,NOMBREEMISOR,TOTAL,FECHA_EMISION,FECHA_RECEPCION,UUID,'ACEPTADO' ESTADO,'' DESCRIPCION FROM CFD_RECEPCION WHERE UUID = @uuid", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@uuid", MySqlDbType.VarChar) { Value = uuid}
                });
            }
            
        }

        public DataTable EstatusUUIDErr(string uuid)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("SELECT '' SERIE,'' FOLIO,'' RFCEMISOR,'' NOMBREEMISOR,0 TOTAL,NOW() FECHA_EMISION,NOW() FECHA_RECEPCION,DESCRIPCION,UUID,'RECHAZADO' ESTADO,'' DESCRIPCION FROM CFD_RECEPCION_ERROR WHERE UUID = @uuid", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@uuid", SqlDbType.VarChar) { Value = uuid}
                });
            }
            else
            {
                return this.conexion.GenericSelectMysql("SELECT '' SERIE,'' FOLIO,'' RFCEMISOR,'' NOMBREEMISOR,0 TOTAL,NOW() FECHA_EMISION,NOW() FECHA_RECEPCION,DESCRIPCION,UUID,'RECHAZADO' ESTADO,'' DESCRIPCION FROM CFD_RECEPCION_ERROR WHERE UUID = @uuid", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@uuid", MySqlDbType.VarChar) { Value = uuid}
                });
            }

        }

    }
}
