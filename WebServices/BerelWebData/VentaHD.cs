﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class VentaHD
    {

        public ConexionBD conexion;

        public VentaHD(ConexionBD con)
        {
            conexion = con;
        }


        public DataTable ObtenerVentaHD()
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("obtenerVentaProcesada");
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("obtenerVentaProcesada");
            }

        }

        public DataTable ObtenerVentaConsolidadaHD()
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("obtenerAcumulados");
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("obtenerAcumulados");
            }

        }

        public DataTable ObtenerUltimaDescarga()
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("obtenerUltimaDescarga");
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("obtenerUltimaDescarga");
            }

        }



    }
}
