﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Usuario
    {
        public ConexionBD conexion;

        public Usuario(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable ObtenerUsuarioLogin(string user,string password,string versionApp)
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerUsuarioLogin", new SqlParameter[]
                {
                    new SqlParameter("@Codigo", SqlDbType.VarChar) { Value = user },
                    new SqlParameter("@Password", SqlDbType.VarChar) { Value = password },
                    new SqlParameter("@VersionApp", SqlDbType.VarChar) { Value = versionApp }
                });
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerUsuarioLogin", new MySqlParameter[]
                {
                    new MySqlParameter("@Codigo_", MySqlDbType.VarChar) { Value = user },
                    new MySqlParameter("@Password_", MySqlDbType.VarChar) { Value = password }
                });
            }
            
        }
      

        public int ValidarUsuario(string usuario, string password)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("ValidarUsuario", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@Usuario", SqlDbType.VarChar) { Value = usuario},
                    new System.Data.SqlClient.SqlParameter("@Password", SqlDbType.VarChar) { Value = password}
                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("ValidarUsuario", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@Usuario", MySqlDbType.VarChar) { Value = usuario},
                    new MySql.Data.MySqlClient.MySqlParameter("@Password", MySqlDbType.VarChar) { Value = password}
                });
            }
            
        }


        public DataTable ObtenerUsuario(string usuario, string password)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerUsuario", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@Usuario", SqlDbType.VarChar) { Value = usuario},
                    new System.Data.SqlClient.SqlParameter("@Password", SqlDbType.VarChar) { Value = password}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerUsuario", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@Usuario", MySqlDbType.VarChar) { Value = usuario},
                    new MySql.Data.MySqlClient.MySqlParameter("@Password", MySqlDbType.VarChar) { Value = password}
                });
            }
            
        }


        public int GuardarUsuario(int nomina,string nombre,int idAgencia,decimal sueldo,int idPuesto,int idSupervisor,int idTienda,string codigo,string password, DateTime fechaIngreso,string usuario,string rutaImagen,int? esMedioTiempo)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("GuardarUsuario", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@nomina", SqlDbType.Int) { Value = nomina},
                    new System.Data.SqlClient.SqlParameter("@nombre", SqlDbType.VarChar) { Value = nombre},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia},
                    new System.Data.SqlClient.SqlParameter("@Sueldo", SqlDbType.Decimal) { Value = sueldo},
                    new System.Data.SqlClient.SqlParameter("@IDPuesto", SqlDbType.Int) { Value = idPuesto},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@EsMedioTiempo", SqlDbType.Int) { Value = esMedioTiempo},
                    new System.Data.SqlClient.SqlParameter("@Codigo", SqlDbType.VarChar) { Value = codigo},
                    new System.Data.SqlClient.SqlParameter("@Password", SqlDbType.VarChar) { Value = password},
                    new System.Data.SqlClient.SqlParameter("@FechaIngreso", SqlDbType.DateTime) { Value = fechaIngreso},
                    new System.Data.SqlClient.SqlParameter("@Usuario", SqlDbType.VarChar) { Value = usuario},
                    new System.Data.SqlClient.SqlParameter("@RutaImagen", SqlDbType.VarChar) { Value = rutaImagen}
                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("GuardarUsuario", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@nomina", MySqlDbType.Int32) { Value = nomina},
                    new MySql.Data.MySqlClient.MySqlParameter("@nombre", MySqlDbType.VarChar) { Value = nombre},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia},
                    new MySql.Data.MySqlClient.MySqlParameter("@Sueldo", MySqlDbType.Decimal) { Value = sueldo},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPuesto", MySqlDbType.Int32) { Value = idPuesto},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@EsMedioTiempo", MySqlDbType.Int32) { Value = esMedioTiempo},
                    new MySql.Data.MySqlClient.MySqlParameter("@Codigo", MySqlDbType.VarChar) { Value = codigo},
                    new MySql.Data.MySqlClient.MySqlParameter("@Password", MySqlDbType.VarChar) { Value = password},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaIngreso", MySqlDbType.DateTime) { Value = fechaIngreso},
                    new MySql.Data.MySqlClient.MySqlParameter("@Usuario", MySqlDbType.VarChar) { Value = usuario},
                    new MySql.Data.MySqlClient.MySqlParameter("@RutaImagen", MySqlDbType.VarChar) { Value = rutaImagen}
                });
            }
            
        }

        public DataTable ObtenerPromotores(int idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerPromotores", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@IDUsuario", SqlDbType.VarChar) { Value = idUsuario}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerPromotores", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@IDUsuario", MySqlDbType.VarChar) { Value = idUsuario}
                });
            }
            
        }

        public DataTable ObtenerUsuarios(DateTime fechaInicio, DateTime fechaFin,int? idSupervisor,int? idAgencia,int? estatus)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerUsuarios", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia},
                    new System.Data.SqlClient.SqlParameter("@Estatus", SqlDbType.Int) { Value = estatus}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerUsuarios", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia},
                    new MySql.Data.MySqlClient.MySqlParameter("@Estatus", MySqlDbType.Int32) { Value = estatus}
                });
            }

            
        }

        public DataTable ObtenerUsuario(int? idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerUsuarioByID", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@IDUsuario", SqlDbType.Int) { Value = idUsuario}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerUsuarioByID", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@IDUsuario", MySqlDbType.Int32) { Value = idUsuario}
                });
            }
            
        }

        public int ActualizarUsuario(int ID, int nomina, string nombre, int idAgencia, decimal sueldo, int idPuesto, int idSupervisor, int idTienda, string codigo,string password, string usuario,string imagen, int esMedioTiempo)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericIntScalarQuery("ActualizarUsuario", new System.Data.SqlClient.SqlParameter[]
                {       
                    new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.Int) { Value = ID},
                    new System.Data.SqlClient.SqlParameter("@Nomina", SqlDbType.Int) { Value = nomina},
                    new System.Data.SqlClient.SqlParameter("@Nombre", SqlDbType.VarChar) { Value = nombre},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia},
                    new System.Data.SqlClient.SqlParameter("@Sueldo", SqlDbType.Decimal) { Value = sueldo},
                    new System.Data.SqlClient.SqlParameter("@IDPuesto", SqlDbType.Int) { Value = idPuesto},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@EsMedioTiempo", SqlDbType.Int) { Value = esMedioTiempo},
                    new System.Data.SqlClient.SqlParameter("@Codigo", SqlDbType.VarChar) { Value = codigo},
                    new System.Data.SqlClient.SqlParameter("@Password", SqlDbType.VarChar) { Value = password},
                    new System.Data.SqlClient.SqlParameter("@Usuario", SqlDbType.VarChar) { Value = usuario},
                    new System.Data.SqlClient.SqlParameter("@Imagen", SqlDbType.VarChar) { Value = imagen}
                });
            }
            else
            {
                return this.conexion.GenericIntScalarQuery("ActualizarUsuario", new MySql.Data.MySqlClient.MySqlParameter[]
                {       
                    new MySql.Data.MySqlClient.MySqlParameter("@ID", MySqlDbType.Int32) { Value = ID},
                    new MySql.Data.MySqlClient.MySqlParameter("@Nomina", MySqlDbType.Int32) { Value = nomina},
                    new MySql.Data.MySqlClient.MySqlParameter("@Nombre", MySqlDbType.VarChar) { Value = nombre},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia},
                    new MySql.Data.MySqlClient.MySqlParameter("@Sueldo", MySqlDbType.Decimal) { Value = sueldo},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPuesto", MySqlDbType.Int32) { Value = idPuesto},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@EsMedioTiempo", MySqlDbType.Int32) { Value = esMedioTiempo},
                    new MySql.Data.MySqlClient.MySqlParameter("@Codigo", MySqlDbType.VarChar) { Value = codigo},
                    new MySql.Data.MySqlClient.MySqlParameter("@Password", MySqlDbType.VarChar) { Value = password},
                    new MySql.Data.MySqlClient.MySqlParameter("@Usuario", MySqlDbType.VarChar) { Value = usuario},
                    new MySql.Data.MySqlClient.MySqlParameter("@Imagen", MySqlDbType.VarChar) { Value = imagen}
                });
            }
            
        }


        public bool ActivarUsuario(int? id, int? estatusPromotor, DateTime fechaBaja)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericBoolScalarQuery("ActivarUsuario", new SqlParameter[]
                {
                    new SqlParameter("@ID", SqlDbType.Int) {Value = id},
                    new SqlParameter("@EstatusPromotor", SqlDbType.Int) {Value = estatusPromotor},
                    new SqlParameter("@FechaBaja", SqlDbType.DateTime) {Value = fechaBaja}
                });
            }
            else
            {
                return this.conexion.GenericBoolScalarQuery("ActivarUsuario", new MySqlParameter[]
                {
                    new MySqlParameter("@ID", MySqlDbType.Int32) {Value = id},
                    new MySqlParameter("@EstatusPromotor", MySqlDbType.Int32) {Value = estatusPromotor},
                    new MySqlParameter("@FechaBaja", MySqlDbType.DateTime) {Value = fechaBaja}
                });
            }

            

        }


    }
}
