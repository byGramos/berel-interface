﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Imagen
    {

        public ConexionBD conexion;

        public Imagen(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable ObtenerImagenID(int idImagen, int esEntrada)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerImagenID", new System.Data.SqlClient.SqlParameter[]
                {
                    new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.Int) { Value = idImagen },
                    new System.Data.SqlClient.SqlParameter("@esEntrada", SqlDbType.Int) { Value = esEntrada }
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerImagenID", new MySql.Data.MySqlClient.MySqlParameter[]
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@ID", MySqlDbType.Int32) { Value = idImagen },
                    new MySql.Data.MySqlClient.MySqlParameter("@esEntrada", MySqlDbType.Int32) { Value = esEntrada }
                });
            }
           
        }


        public DataTable ObtenerImagenPerfil(int idImagen)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerImagenPerfil", new System.Data.SqlClient.SqlParameter[]
                {
                    new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.Int) { Value = idImagen }
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerImagenPerfil", new MySql.Data.MySqlClient.MySqlParameter[]
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@ID", MySqlDbType.Int32) { Value = idImagen }
                });
            }

        }



    }
}
