﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BerelWebData
{
    public class Reporte
    {

        public ConexionBD conexion;

        public Reporte(ConexionBD con)
        {
            conexion = con;
        }

        public DataTable ObtenerReporteAsistencias(DateTime fechaInicio, DateTime fechaFin,int?idSupervisor,int? idTienda,int? idPromotor)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteAsistencias", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteAsistencias", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor}
                });
            }
            
        }

        public DataTable ObtenerReporteTransferencias(int? idSupervisor,int? idEstatus,DateTime fechaInicio, DateTime fechaFin)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteTransferencias", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@IDUsuario", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDEstatus", SqlDbType.Int) { Value = idEstatus},
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteTransferencias", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@IDUsuario", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDEstatus", MySqlDbType.Int32) { Value = idEstatus},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin}
                });
            }
            
        }


        public DataTable ObtenerTransferenciasByID(int? idSupervisor)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerTransferenciasByID", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@IDUsuario", SqlDbType.Int) { Value = idSupervisor}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerTransferenciasByID", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@IDUsuario", MySqlDbType.Int32) { Value = idSupervisor}
                });
            }
            
        }


        public DataTable ObtenerReporteHorarios(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor,int? idAgencia)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteHorarioPromotores", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteHorarioPromotores", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia}
                });
            }

        }

        public DataTable ObtenerReporteHorarios2(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor, int? idAgencia)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteHorarioPromotoresTodo", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteHorarioPromotores", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia}
                });
            }

        }


        public DataTable ObtenerReporteNomina(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor,int? idAgencia)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteResumenNominaPromotores", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor},
                    new System.Data.SqlClient.SqlParameter("@IDAgencia", SqlDbType.Int) { Value = idAgencia}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteResumenNominaPromotores", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDAgencia", MySqlDbType.Int32) { Value = idAgencia}
                });
            }

        }


        public DataTable ObtenerReporteComisiones(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ReporteComisionesPromotores", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ReporteComisionesPromotores", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor}
                });
            }

        }

        public DataTable ObtenerIncidenciasParciales(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerIncidenciasParciales", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerIncidenciasParciales", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor}
                });
            }

        }


        public DataTable ObtenerIncidenciasPorAprobar(DateTime fechaInicio, DateTime fechaFin, int? idSupervisor, int? idTienda, int? idPromotor)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerIncidenciasPorAprobar", new System.Data.SqlClient.SqlParameter[]
                {   
                    new System.Data.SqlClient.SqlParameter("@FechaInicio", SqlDbType.Date) { Value = fechaInicio},
                    new System.Data.SqlClient.SqlParameter("@FechaFin", SqlDbType.Date) { Value = fechaFin},
                    new System.Data.SqlClient.SqlParameter("@IDSupervisor", SqlDbType.Int) { Value = idSupervisor},
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.Int) { Value = idTienda},
                    new System.Data.SqlClient.SqlParameter("@IDPromotor", SqlDbType.Int) { Value = idPromotor}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerIncidenciasPorAprobar", new MySql.Data.MySqlClient.MySqlParameter[]
                {   
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaInicio", MySqlDbType.Date) { Value = fechaInicio},
                    new MySql.Data.MySqlClient.MySqlParameter("@FechaFin", MySqlDbType.Date) { Value = fechaFin},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDSupervisor", MySqlDbType.Int32) { Value = idSupervisor},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.Int32) { Value = idTienda},
                    new MySql.Data.MySqlClient.MySqlParameter("@IDPromotor", MySqlDbType.Int32) { Value = idPromotor}
                });
            }

        }



    }
}
