﻿using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System;

namespace BerelWebData
{
    public class ConexionBD
    {
        protected SqlConnection Conexion { get; set; }
        protected SqlCommand Comando { get; set; }
        protected SqlDataAdapter Adaptador { get; set; } //Conexion offline
        protected SqlDataReader Lector { get; set; } //Conexion Online
        
        protected MySqlConnection ConexionMysql { get; set; }
        protected MySqlCommand ComandoMysql { get; set; }
        protected MySqlDataAdapter AdaptadorMysql { get; set; } //Conexion offline
        protected MySqlDataReader LectorMysql { get; set; } //Conexion Online

        protected SqlTransaction Transaction { get; set; }  
        protected MySqlTransaction TransactionMysql { get; set; }
        public bool TransaccionIniciada { get; set; }
        public string activeConnection { get; set; }

        public ConexionBD(string engineDB)
        {
            //activeConnection = ConfigurationManager.AppSettings["ActiveConnection"];
            activeConnection = engineDB;

            if (activeConnection == "SQL") // Base de datos SQL
            {
                Conexion = new SqlConnection(getConnectionString(activeConnection));
                Comando = new SqlCommand();
                Comando.Connection = Conexion;
                Comando.CommandType = CommandType.StoredProcedure;
                Adaptador = new SqlDataAdapter(Comando);
            }
            else
            {
                ConexionMysql = new MySqlConnection(getConnectionString(activeConnection));
                ComandoMysql = new MySqlCommand();
                ComandoMysql.Connection = ConexionMysql;
                ComandoMysql.CommandType = CommandType.StoredProcedure;
                AdaptadorMysql = new MySqlDataAdapter(ComandoMysql);

            }
            
        }


        string getConnectionString(string activeConnection)
        {
            var connection = "";
            //var activeConnection = ConfigurationManager.AppSettings["ActiveConnection"];

            if (activeConnection == null)
            {
                throw new Exception("No se ha especificado \"ActiveConnection\" en el WebConfig");
            }
            else
            {
                connection = ConfigurationManager.ConnectionStrings[activeConnection].ConnectionString;
            }

            return connection;
        }


        public void BeginTransaction()
        {
            if(activeConnection == "SQL")
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }
                Transaction = Conexion.BeginTransaction();
                this.Comando.Transaction = Transaction;
                TransaccionIniciada = true;
            }
            else
            {
                if (ConexionMysql.State == ConnectionState.Closed)
                {
                    ConexionMysql.Open();
                }
                TransactionMysql = ConexionMysql.BeginTransaction();
                this.ComandoMysql.Transaction = TransactionMysql;
                TransaccionIniciada = true;
            }
            
        }

        public void Commit()
        {
            if(activeConnection == "SQL")
            {
                if (TransaccionIniciada)
                {
                    Transaction.Commit();
                    this.Comando.Transaction = null;
                    this.Conexion.Close();
                    TransaccionIniciada = false;
                }
            }
            else
            {
                if (TransaccionIniciada)
                {
                    TransactionMysql.Commit();
                    this.ComandoMysql.Transaction = null;
                    this.ConexionMysql.Close();
                    TransaccionIniciada = false;
                }
            }
            
        }

        public void Rollback()
        {
            if (activeConnection == "SQL")
            {
                if (TransaccionIniciada)
                {
                    Transaction.Rollback();
                    this.Comando.Transaction = null;
                    this.Conexion.Close();
                    TransaccionIniciada = false;
                }
            }
            else
            {
                if (TransaccionIniciada)
                {
                    TransactionMysql.Rollback();
                    this.ComandoMysql.Transaction = null;
                    this.ConexionMysql.Close();
                    TransaccionIniciada = false;
                }
            }
            
        }

        protected void close()
        {
            if (activeConnection == "SQL")
            {
                if (!TransaccionIniciada)
                {
                    if (Conexion.State == ConnectionState.Open)
                    {
                        Conexion.Close();
                    }
                }
            }
            else
            {
                if (!TransaccionIniciada)
                {
                    if (ConexionMysql.State == ConnectionState.Open)
                    {
                        ConexionMysql.Close();
                    }
                }
            }
            
        }

        /// <summary>
        /// Consume un SP que regresa un SELECT
        /// </summary>
        /// <param name="spName">Nombre del SP a Consumir</param>
        /// <param name="parameters">Array de SqlParameter que se pasaran como parametro al SP</param>
        /// <returns></returns>
        public DataTable GenericSPSelect(string spName, SqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();

            try
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }

                Comando.CommandText = spName;
                Comando.Parameters.Clear();
                if (parameters != null)
                {
                    Comando.Parameters.AddRange(parameters);
                }

                Adaptador.Fill(data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return data;
        }

        /// <summary>
        /// Consume un SP MYSQL que regresa un SELECT
        /// </summary>
        /// <param name="spName">Nombre del SP a Consumir</param>
        /// <param name="parameters">Array de SqlParameter que se pasaran como parametro al SP</param>
        /// <returns></returns>
        public DataTable GenericSPSelectMysql(string spName, MySqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();

            try
            {
                if (ConexionMysql.State == ConnectionState.Closed)
                {
                    ConexionMysql.Open();
                }

                ComandoMysql.CommandText = spName;
                ComandoMysql.Parameters.Clear();
                if (parameters != null)
                {
                    ComandoMysql.Parameters.AddRange(parameters);
                }

                AdaptadorMysql.Fill(data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return data;
        }

        public bool GenericBoolScalarQuery(string spName, SqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();
            bool resp = false;

            try
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }

                Comando.CommandText = spName;
                Comando.Parameters.Clear();
                if (parameters != null)
                {
                    Comando.Parameters.AddRange(parameters);
                }

                Adaptador.Fill(data);

                if (data.Rows.Count > 0)
                {
                    resp = data.Rows[0][0].ToString() == "1" || Convert.ToBoolean(data.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return resp;
        }

        /// <summary>
        /// Metodo para operaciones de inserción y modificacion a BAse de datos MYSQL
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool GenericBoolScalarQuery(string spName, MySqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();
            bool resp = false;

            try
            {
                if (ConexionMysql.State == ConnectionState.Closed)
                {
                    ConexionMysql.Open();
                }

                ComandoMysql.CommandText = spName;
                ComandoMysql.Parameters.Clear();
                if (parameters != null)
                {
                    ComandoMysql.Parameters.AddRange(parameters);
                }

                AdaptadorMysql.Fill(data);

                if (data.Rows.Count > 0)
                {
                    resp = data.Rows[0][0].ToString() == "1" || Convert.ToBoolean(data.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return resp;
        }

        public int GenericIntScalarQuery(string spName, SqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();
            int resp = 0;

            try
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }

                Comando.CommandText = spName;
                Comando.Parameters.Clear();
                if (parameters != null)
                {
                    Comando.Parameters.AddRange(parameters);
                }

                Adaptador.Fill(data);

                if (data.Rows.Count > 0)
                {
                    resp = Convert.ToInt32(data.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return resp;
        }

        /// <summary>
        /// MEtodo para realizar inserciones en Base de datos MYSQL
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int GenericIntScalarQuery(string spName, MySqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();
            int resp = 0;

            try
            {
                if (ConexionMysql.State == ConnectionState.Closed)
                {
                    ConexionMysql.Open();
                }

                ComandoMysql.CommandText = spName;
                ComandoMysql.Parameters.Clear();
                if (parameters != null)
                {
                    ComandoMysql.Parameters.AddRange(parameters);
                }

                AdaptadorMysql.Fill(data);

                if (data.Rows.Count > 0)
                {
                    resp = Convert.ToInt32(data.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return resp;
        }


        public DataTable GenericSelectMysql(string spName, MySqlParameter[] parameters = null)
        {
            DataTable data = new DataTable();

            try
            {
                if (ConexionMysql.State == ConnectionState.Closed)
                {
                    ConexionMysql.Open();
                }
                //command = new SqlCommand("SELECT * FROM Table WHERE ID=@someID",connection)
                ComandoMysql.CommandText = spName;
                ComandoMysql.CommandType = CommandType.Text;
                ComandoMysql.Parameters.Clear();
                if (parameters != null)
                {
                    ComandoMysql.Parameters.AddRange(parameters);
                }

                AdaptadorMysql.Fill(data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                this.close();
            }

            return data;
        }


    }
}
