﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Threading.Tasks;


namespace BerelWebData
{
    public class CatalogoGeneral
    {
        public ConexionBD conexion;

        public CatalogoGeneral(ConexionBD con)
        {
            conexion = con;
        }


        public DataTable ObtenerSupervisores()
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerSupervisores");
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerSupervisores");
            }
            
        }

        public DataTable ObtenerTiposIncidencias()
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerTiposIncidencias");
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerTiposIncidencias");
            }
            
        }

        public DataTable ObtenerAgencias()
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerAgencias");
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerAgencias");
            }
            
        }

        public DataTable ObtenerPuestos()
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerPuestos");
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerPuestos");
            }
            
        }

        public DataTable ObtenerTiendasAsignadas(int? idUsuario)
        {
            if (conexion.activeConnection == "SQL")
            {
                return conexion.GenericSPSelect("ObtenerTiendasAsignadas", new SqlParameter[]
                {
                    new SqlParameter("@IDUsuario", SqlDbType.Int) {Value = idUsuario}
                });
            }
            else
            {
                return conexion.GenericSPSelectMysql("ObtenerTiendasAsignadas", new MySqlParameter[]
                {
                    new MySqlParameter("@IDUsuario", MySqlDbType.Int32) {Value = idUsuario}
                });
            }
            
        }

        public DataTable ObtenerPromotoresByTienda(int? idTienda)
        {
            if (conexion.activeConnection == "SQL")
            {
                return this.conexion.GenericSPSelect("ObtenerPromotoresByTienda", new System.Data.SqlClient.SqlParameter[]
                {                    
                    new System.Data.SqlClient.SqlParameter("@IDTienda", SqlDbType.VarChar) { Value = idTienda}
                });
            }
            else
            {
                return this.conexion.GenericSPSelectMysql("ObtenerPromotoresByTienda", new MySql.Data.MySqlClient.MySqlParameter[]
                {                    
                    new MySql.Data.MySqlClient.MySqlParameter("@IDTienda", MySqlDbType.VarChar) { Value = idTienda}
                });
            }
            
        }
     

    }
}
