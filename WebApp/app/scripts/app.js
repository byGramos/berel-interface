'use strict';

/**
 * @ngdoc overview
 * @name berelWebApp
 * @description
 * # berelWebApp
 *
 * Main module of the application.
 */
angular
  .module('berelWebApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'angularMoment',
    'angular-loading-bar',
    'checklist-model',
    'oitozero.ngSweetAlert',
    'ngTable',
    'ngCsv',
    'ui.select',    
    'uiGmapgoogle-maps',
    'ui.bootstrap',
    'naif.base64'
  ])
  .config(function ($stateProvider,$urlRouterProvider,configuracion,uiGmapGoogleMapApiProvider) {
    
    // Clave API Generada con cuenta gdelangel83 (Cambiar por la de Berel)
      uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCq7m7hLlC2WghbfVamaHjW9zL5dszjgMc'
        //key: 'AIzaSyAxFEpm4QLkSAHGm5RVwtbFeChD60AWaZk'
    });


    //$urlRouterProvider.otherwise('/inicio');

      $stateProvider
        .state('inicio',{
            url:'/inicio',
            templateUrl:'views/inicio.html',
            controller:'InicioCtrl',
            loginRequired: true,
            cache:false
        })
        .state('login',{
            url:'/login',
            templateUrl:'views/login.html',
            controller:'LoginCtrl',
            cache:false
        })
        .state('ventaHomeDepot',{
            url:'/ventaHomeDepot/:menu',
            templateUrl:'views/reporteVentaHD.html',
            controller:'VentahomedepotCtrl',
            cache:false
        })
        .state('consultaUUID',{
            url:'/consultaUUID/:menu',
            templateUrl:'views/EstatusUUID.html',
            controller:'KoneshCtrl',
            cache:false
        })
        .state('ReporteComisiones',{
            url:'/ComisionesPromotores',
            templateUrl:'views/reporteComisiones.html',
            controller:'ReportecomisionesCtrl',
            cache:false
        })
        .state('ReportePromo',{
            url:'/ReportePromo',
            templateUrl:'views/reportePromo.html',
            controller:'ReportepromoCtrl',
            cache:false
        })
        .state('ReportePedidoPromo',{
            url:'/ReportePedidoPromo',
            templateUrl:'views/reporteEstatusPedidos.html',
            controller:'ReportepromoCtrl',
            cache:false
        });

  })
  .run(['$rootScope', '$location', 'autenticacion', '$http', '$state', 'configuracion', 'BERConfig', 
        function($rootScope, $location, autenticacion, $http, $state, configuracion, BERConfig) {
            moment.locale('es');
            
            // Se define prototipo setObject y getObject para LocalStorage
            if (window.Storage) {
                window.Storage.prototype.setObject = function(key, obj) {
                    this.setItem(configuracion.IDSistema + '_' + key, JSON.stringify(obj));
                };

                window.Storage.prototype.getObject = function(key) {
                    var value = this.getItem(configuracion.IDSistema + '_' + key);
                    var obj = value === null ? null : JSON.parse(value);
                    return obj;
                };
            }

            if (autenticacion.isLogged()) {
                var usuarioActivo = BERConfig.getUsuarioActivo();
                BERConfig.setPermisos(usuarioActivo.Permisos,usuarioActivo.IDPuesto);
            }
            else{
                //$state.go('login');
                $location.path('/login');
                return;
            }

            $rootScope.$on('$stateChangeStart', function(event, toState) {
                if (toState.url === '/login' && autenticacion.isLogged()) {
                    event.preventDefault();
                    $location.path('/asistencia');
                    return;
                }

                if (toState.loginRequired && !autenticacion.isLogged()) {
                    event.preventDefault();
                    $rootScope.returnToState = toState.url;
                    $state.go('login');
                }

                
            });

            moment.fn.microsoftDate = function() {
                return '/Date(' + this.valueOf() + '-0700)/';
            };
        }

  ]);
