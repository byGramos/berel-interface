'use strict';

angular.module('modalLoading', ['ui.bootstrap'])
    .service('$loading', ['$injector',
        function($injector) {
            var $modal;
            var modal;
            var loadingsOpenned = 0;
            var preventOneCount = 0;
            var turnOnOnceCount = 0;

            var isOn = true;

            /*var isPreventOne = function() {
                var isPrevent = false;
                if (preventOneCount > 0) {
                    isPrevent = true;
                    preventOneCount--;
                }

                return isPrevent;
            };

            var isTurnOnce = function() {
                var isTurn = false;
                if (turnOnOnceCount > 0) {
                    isTurn = true;
                    turnOnOnceCount--;
                }

                return isTurn;
            };*/

            return {
                preventOne: function() {
                    preventOneCount++;
                },
                preventAll: function() {
                    isOn = false;
                },
                turnOnOnce: function() {
                    turnOnOnceCount++;
                },
                turnOnAll: function() {
                    isOn = true;
                },
                showLoading: function() {
                    if (!$modal) {
                        $modal = $injector.get('$uibModal');
                    }

                    if(isOn){
                    //if (((isTurnOnce() || isOn) && !isPreventOne())) {

                        if (loadingsOpenned === 0) {
                            modal = $modal.open({
                                animation: true,
                                templateUrl: 'views/template/modalLoading.html',
                                size: 'sm',
                                windowClass: 'modal-loading',
                                keyboard: true,
                                backdrop: 'static'
                            });
                        }

                        loadingsOpenned++;
                    }
                },
                hideLoading: function() {
                    if (modal && loadingsOpenned > 0) {
                        loadingsOpenned--;
                    }
                    if (modal && loadingsOpenned === 0) {

                        modal.dismiss();
                    }
                },
                setModal: function(m) {
                    $modal = m;
                }
            };
        }
    ])
    .factory('$httpInterceptLoading', ['$q', '$loading', '$injector', 'configuracion', function($q, $loading, $injector, configuracion) {
        return {
            'request': function(config) {
                if (config.url.indexOf(configuracion.DireccionActiva) === 0 && config.url.indexOf('Usuario/ValidarSesionActiva') === -1 && config.url.search(/PDV\/Sucursal\/\d\/Buscar/) === -1) {
                    $loading.setModal($injector.get('$uibModal'));
                    $loading.showLoading();
                }
                return config;
            },
            'response': function(response) {
                if (response.config.url.indexOf(configuracion.DireccionActiva) === 0  && response.config.url.indexOf('Usuario/ValidarSesionActiva') === -1 && response.config.url.search(/PDV\/Sucursal\/\d\/Buscar/) === -1) {
                    $loading.hideLoading();
                }
                return response;
            },
            'responseError': function(rejection) {
                if (rejection.config.url.indexOf(configuracion.DireccionActiva) === 0 && rejection.config.url.indexOf('Usuario/ValidarSesionActiva') === -1 && rejection.config.url.search(/PDV\/Sucursal\/\d\/Buscar/) === -1) {
                    $loading.hideLoading();
                }
                return $q.reject(rejection);
            }
        };
    }])
    .config(['$httpProvider',
        function($httpProvider) {
            $httpProvider.interceptors.push('$httpInterceptLoading');
        }
    ]);
