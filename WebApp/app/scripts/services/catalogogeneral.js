'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.catalogoGeneral
 * @description
 * # catalogoGeneral
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
  .service('catalogoGeneral', ['$http', '$q', 'configuracion', 'BERConfig',
    function ($http, $q, configuracion, BERConfig) {
      var url = configuracion.DireccionActiva;
      return {
        ObtenerSupervisores: function () { 
          return $q(function (resolve, reject) {
            $http.get(url + '/CatalogoGeneral/Web/ObtenerSupervisores').then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
         obtenerPromotoresByTienda: function (idTienda) { 
           var params = {
            idTienda : idTienda
          };

          return $q(function (resolve, reject) {
            $http.get(url + '/CatalogoGeneral/Web/ObtenerPromotoresByTienda',{ params:params }).then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
         obtenerTiposIncidencias: function () {
          return $q(function (resolve, reject) {
            $http.get(url + '/CatalogoGeneral/Web/ObtenerTiposIncidencias').then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
        obtenerAgencias: function () {
          return $q(function (resolve, reject) {
            $http.get(url + '/CatalogoGeneral/Web/ObtenerAgencias').then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
        obtenerPuestos: function () {
          return $q(function (resolve, reject) {
            $http.get(url + '/CatalogoGeneral/Web/ObtenerPuestos').then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
        obtenerTiendasAsignadas: function (id) {
          return $q(function (resolve, reject) {
            $http.post(url + '/CatalogoGeneral/Web/ObtenerTiendasAsignadas?id=' + id).then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
        obtenerPaises: function () {
          return $q(function (resolve, reject) {
            var objPais = [
              {
                ID: 1,
                Nombre: 'Mexico'
              }
            ];

            resolve(objPais);


          });
        },
        obtenerEstados: function (idPais) {
          return $q(function (resolve, reject) {
            $http.get(url + '/Estado').then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        },
        obtenerMunicipios: function (idPais, idEstado) {
          return $q(function (resolve, reject) {
            $http.get(url + '/Municipio/?id_estado=' + idEstado )
            .then(function (resp) {
              resolve(resp.data.Datos);
            }, function (err) {
              reject(err.data, err.status);
            });
          });
        }, 
        buscarCuenta: function (numeroNomina) {
          var params = {
            numero_nomina: numeroNomina
          };

          return $q(function (resolve, reject) {
            $http.get(configuracion.DireccionActiva + 'Prestamo/BuscarCuenta/?numero_nomina=' + numeroNomina)
              .then(function (resp) {
                resolve(resp.data.Datos);
              }, function (err) {
                reject(err.data, err.status);
              });
          });
        }


      };
    }]);
