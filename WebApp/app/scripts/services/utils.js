'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.utils
 * @description
 * # utils
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
  .service('utils', function($uibModal,$http,$q,reporte,$location,$rootScope) {
        

        this.modalEvidencia = function(idImagen, arregloImagenes,esEntrada) {
            $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'views/template/modalImagenEvidencia.html',
                controller: ['$scope', '$uibModalInstance', function($scp, $uibModalInstance) {

                    var index = 0;

                    function calcularImagenAnterior() {
                        return index - 1;
                    }

                    function calcularImagenSiguiente() {
                        var value = index + 1;
                        if (value === arregloImagenes.length) {
                            value = -1;
                        }
                        return value;
                    }

                    function validarBotones() {
                        var registroAnterior = calcularImagenAnterior();
                        var registroSiguiente = calcularImagenSiguiente();
                        $scp.showPrevious = registroAnterior >= 0 ? true : false;
                        $scp.showNext = registroSiguiente >= 0 ? true : false;
                        $scp.registro = arregloImagenes[index];
                    }

                    function buscarRegistro() {
                        for (var i = 0; i < arregloImagenes.length; i++) {
                            if (arregloImagenes[i].IDImagen === idImagen) {
                                index = i;
                                validarBotones();
                            }
                        }
                    }

                    $scp.showPrevious = false;
                    $scp.showNext = false;
                    $scp.rutaImagen = reporte.urlEvidencia(idImagen,esEntrada);
                    buscarRegistro();


                    $scp.next = function() {
                        index = calcularImagenSiguiente();
                        $scp.rutaImagen = reporte.urlEvidencia(arregloImagenes[index].IDImagen,esEntrada);
                        validarBotones();
                    };

                    $scp.previous = function() {
                        index = calcularImagenAnterior();
                        $scp.rutaImagen = reporte.urlEvidencia(arregloImagenes[index].IDImagen,esEntrada);
                        validarBotones();
                    };

                    $scp.close = function() {
                        $uibModalInstance.dismiss('cancel');
                    };

                }],
            });
        };

        // Recibo el registro seleccionado en la vista 
        this.modalGeolocation = function(obj) {
            $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'views/template/modalGoogleMaps.html',
                controller: ['$scope', '$uibModalInstance','uiGmapIsReady', function($scope, $uibModalInstance,uiGmapIsReady) {

                    var index = 0;
                    $scope.mapLoad = false;   

                    $scope.tituloPagina = obj.Tienda;
                    var coordTienda = obj.GPSTienda.split(",");
                    var lat = coordTienda[0];//25.718195;
                    var lng = coordTienda[1];//-100.519200;

                    // Se debe centrar el mapa sobre la ubicacion de la tienda
                    $scope.map = {
                    center: {
                        latitude: coordTienda[0],//25.716514,
                        longitude: coordTienda[1]//-100.5189538
                    },
                    zoom: 16,
                    control: {},
                    options: {
                        scrollwheel: false
                    }
                };
                
                $scope.map.registro = {}; 
                $scope.map.registro = obj; 
                 //var markers = [];                   
                $scope.map.markers = [];   

                    function refreshMap(latitude, longitude) {
                        if ($scope.map) {
                            $scope.map.control.refresh({
                                latitude: latitude,
                                longitude: longitude
                            });
                        }
                    }

                    function buscarRegistro() {
                        for (var i = 0; i < arregloGPS.length; i++) {
                            if (arregloGPS[i].IDVisita === idVisita) {
                                index = i;
                                $scope.tituloPagina = arregloGPS[index].Cliente;
                                $scope.map.registro = arregloGPS[index];
                                //validarBotones();
                                configurarMapa();
                            }
                        }
                    }

                    function configurarMapa(){

                        var coord = [];

                        coord = $scope.map.registro.GPSEntrada.split(",");
                        //var coordTienda = obj.GPSTienda.split(",");

                        // se debe cargar el marker de la tienda seleccionada
                        var tienda = {
                        latitude: coordTienda[0],//25.718223, 
                        longitude: coordTienda[1], //-100.519954, 
                        title: "m0", 
                        id: 0, 
                        icon:'images/marker_THD.png',
                        Cliente: {
                            Nombre: $scope.map.registro.Tienda,
                            latitud: parseFloat(coordTienda[0]),//'25.718223', //VAlor dinamico
                            longitud: parseFloat(coordTienda[1]), //'-100.519954' // Valor dinamico
                            Entrada: '-',
                            Salida: '-',
                            Distancia: $scope.map.registro.Rango >= 1000 ?  parseFloat($scope.map.registro.Rango/1000).toFixed(2) : parseFloat($scope.map.registro.Rango).toFixed(2),
                            Unidad: $scope.map.registro.Rango >= 1000 ? 'Km' : 'm'
                        },
                        options: {labelClass:'marker_labels',labelAnchor:'5 35'},
                        show: false
                        };

                        var checkIn = {
                        latitude: coord[0], // 25.7148558, 
                        longitude: coord[1], // -100.5182862, 
                        title: "m1", 
                        id: 1, 
                        icon:'images/marker_blue.png',
                        Cliente: {
                            Nombre: "Check In",
                            latitud: parseFloat(coord[0]),
                            longitud: parseFloat(coord[1]),
                            Entrada:  $scope.map.registro.HoraEntrada,
                            Salida:  $scope.map.registro.HoraSalida,
                            Distancia: $scope.map.registro.Rango >= 1000 ?  parseFloat($scope.map.registro.Rango/1000).toFixed(2) : parseFloat($scope.map.registro.Rango).toFixed(2),
                            Unidad: $scope.map.registro.Rango >= 1000 ? 'Km' : 'm'
                        },
                        options: {labelClass:'marker_labels',labelAnchor:'5 35'},
                        show: false
                    };
                    
                        $scope.map.markers.push(tienda);
                        $scope.map.markers.push(checkIn);

                    }          

                    $scope.ok = function() {
                        $uibModalInstance.close({
                            lat: $scope.marker.coords.latitude,
                            lng: $scope.marker.coords.longitude
                        });
                    };

                    $scope.cancel = function() {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.windowOpen = function(marker, eventName, obj) {
                        $scope.map.markers.forEach(function(el) {
                            if (el.id !== obj.id) {
                                el.show = false;
                            }
                        });
                    };
                    
                    //buscarRegistro();

                    $uibModalInstance.rendered
                    .then(function() {
                        uiGmapIsReady.promise()
                            .then(function() {
                                refreshMap(lat, lng);
                                //buscarRegistro();
                                configurarMapa();
                                $scope.mapLoad = true;
                            });

                    });


                    $scope.randomMarkers = $scope.map.markers;//markers;                    

                }],
            });
        };

        this.modalGeolocationPromise = function(idVisita, arregloGPS) {

           $location.path($rootScope.returnToState || '/map');
        };


        this.getImagenPromise = function(idImagen) {
            
            return reporte.urlImagenPerfil(idImagen);
        };


        this.modalPerfil = function(rutaImagenPerfil) {
            $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'views/template/modalImagenEvidencia.html',
                controller: ['$scope', '$uibModalInstance', function($scp, $uibModalInstance) {

                    $scp.rutaImagen = rutaImagenPerfil;
                    
                    $scp.close = function() {
                        $uibModalInstance.dismiss('cancel');
                    };

                }],
            });
        };

        this.modalAyuda = function(modulo) {
            $uibModal.open({
                animation: true,
                size: 'md',
                templateUrl: 'views/template/modalAyuda.html',
                controller: ['$scope', '$uibModalInstance', function($scp, $uibModalInstance) {

                    $scp.modulo = modulo;
                    
                    $scp.close = function() {
                        $uibModalInstance.dismiss('cancel');
                    };


                }],
            });
        };
  
  //////////////////////////////////////////////////////////////////////////////77777


});
