'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.autenticacion
 * @description
 * # autenticacion
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
  .service('autenticacion', ['$rootScope','configuracion','$window','$q','$http','$log','$interval','BERConfig',
        function($rootScope,configuracion, $window, $q, $http, $log, $interval,BERConfig,credenciales) {

            var ip = configuracion.DireccionActiva; //obtiene la direccion activa donde consumira los servicios

            return {
                isLogged:function() { // Valida si el usuario esta logueado
                    var islogged = $window.localStorage.getObject('islogged');

                    return islogged === null ? false : islogged;
                },
                signIn: function(user, pass) { // realiza el registro de la sesion
                    return $q(function(resolve, reject) {
                        var idSistema = configuracion.IDSistema;

                        var usuario = {
                            User: user,
                            Password: pass,
                            IDSistema: idSistema
                        };
                        
                    
                        $http.post(configuracion.DireccionLogin + 'Usuario/IniciarSesion', usuario) // se requiere crear el servicio JS
                            .then(function(resp) {
                                if(resp.data.Estatus > 0){
                                    
                                    $window.localStorage.setObject('loginHeaders', {
                                    token: CryptoJS.MD5(resp.data.Datos.User),
                                    user: resp.data.Datos.User,
                                    idsistema: idSistema
                                });

                                $window.localStorage.setObject('islogged', true);

                                resp.data.Datos.NombreCompleto = resp.data.Datos.Nombre + ' ' + resp.data.Datos.Apellidos;

                                }                                

                                resolve(resp.data.Datos);

                            }, function(err) {
                                if (err.status < 0) {
                                    reject('Sin conexion a los servicios.', err.status);
                                } else {
                                    $log.info(err);
                                    reject(err.data.Mensaje, err.status);
                                }
                            });

                    });
                },
                signOut: function() { // funcion para cerrar Sesion
                    return $q(function(resolve, reject) {

                        if($window.localStorage.getObject('islogged')){
                            $window.localStorage.setObject('islogged', false);
                            BERConfig.setUsuarioActivo(null);
                            BERConfig.removePermisos();
                            resolve();
                        }
                        else{
                            reject("No existe la session", 401);
                        }

                    });
                },
                getLoginHeaders: function() {
                return $window.localStorage.getObject('loginHeaders');
                },
                checkAutoKeepAlive: function(miliseconds, callback) { // Valida en todo momento que la sesion este activa y que no se halla terminado el tiempo
                    callback = typeof miliseconds === 'function' ? miliseconds : undefined;

                    miliseconds = typeof miliseconds === 'number' ? miliseconds : 120000;

                    var check = function() {
                        if($window.localStorage.getObject('islogged')){
                            callback(true);
                        }
                        else{
                            callback(false);
                        }
                        
                    };

                    check();
                    $interval(function() {
                        //if($window.localStorage.getObject('islogged')){
                        check();
                        //}

                    }, miliseconds);
                }

            };
            
            }
        ])
        .service('sesion', function($window) {

                this.isLogged = function() {
                    var islogged = $window.localStorage.getObject('islogged');

                    return islogged === null ? false : islogged;
                };

                this.getLoginHeaders = function() {
                    return $window.localStorage.getObject('loginHeaders');
                };


            })
            .service('interceptorHeadersSesion', function($q, configuracion, sesion) {
                return {
                    request: function(config) {
                        if (config.url.indexOf(configuracion.DireccionLogin) !== 0) {
                            return config;
                        }
                        var sesionActual = sesion.isLogged();
                        if (sesionActual) {
                            var headers = sesion.getLoginHeaders();
                            angular.merge(config.headers, headers);
                        }
                        return config;
                    },
                    responseError: function(config) {
                        return $q.reject(config);
                    },
                };
            })
        .config(function($httpProvider) {
            $httpProvider.interceptors.push('interceptorHeadersSesion');
        });
