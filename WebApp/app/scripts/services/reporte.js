'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.reporte
 * @description
 * # reporte
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
    .service('reporte', ['$q', '$http', 'BERConfig', 'configuracion', '$window',
        function ($q, $http, BERConfig, configuracion, $window) {

            var url = configuracion.DireccionActiva;//RutaImagenes;

            this.obtenerVentaHomeDepot = function (tipo) {
                if (tipo === 1) {
                    return $http.get(configuracion.DireccionActiva + 'Reportes/VentaHomeDepot')
                        .then(function (resp) {
                            return $q.when(resp.data.Datos);
                        })
                        .catch(function (err) {
                            return $q.reject(err);
                        });
                }
                else {
                    return $http.get(configuracion.DireccionActiva + 'Reportes/VentaConsolidadaHD')
                        .then(function (resp) {
                            return $q.when(resp.data.Datos);
                        })
                        .catch(function (err) {
                            return $q.reject(err);
                        });
                }

            };

            this.obtenerUltimaDescarga = function (tipo) {
                return $http.get(configuracion.DireccionActiva + 'Reportes/UltimaDescarga')
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.descargarReporteGenerado = function (guid, fileName) {
                $window.location = url + 'Reportes/xlsx/' + guid + (fileName ? '?fileName=' + fileName : '');
            };

            this.consultarUUID = function (uuid) {
                var params = {
                    uuid: uuid
                };

                return $http.get(configuracion.DireccionActiva + 'Konesh/EstatusUUID', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };


            this.obtenerReporteEvidencias = function (idRuta, fechaInicio, fechaFin) {
                fechaInicio = moment(fechaInicio).format('YYYY-MM-DD');
                fechaFin = moment(fechaFin).format('YYYY-MM-DD');

                return $http.get(url + 'Reporte/Evidencias/Ruta/' + idRuta + '/FechaInicio/' + fechaInicio + '/FechaFin/' + fechaFin + '/Sucursal/' + idSucursal)
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };


            this.obtenerReporteConsultaVisitas = function (idRuta, fechaInicio, fechaFin) {
                fechaInicio = moment(fechaInicio).format('YYYY-MM-DD');
                fechaFin = moment(fechaFin).format('YYYY-MM-DD');

                return $http.get(url + 'Reporte/ConsultaVisitas/Ruta/' + idRuta + '/FechaInicio/' + fechaInicio + '/FechaFin/' + fechaFin)
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };


            this.urlEvidencia = function (idEvidencia, esEntrada) {
                var path = url + 'imagen/' + idEvidencia + '/file/' + esEntrada + '/';
                return path;
            };

            this.urlImagenPerfil = function (idImagen) {
                var path = url + 'imagen/' + idImagen + '/file/';
                return path;
            };


            this.descargarReporteGenerado = function (guid, fileName) {
                $window.location = url + 'Reportes/xlsx/' + guid + (fileName ? '?fileName=' + fileName : '');
            };

            this.obtenerReporteHorarios = function (fechaInicio, fechaFin, idsupervisor, idtienda, idpromotor, idagencia) {
                var params = {
                    fecha_inicio: fechaInicio,
                    fecha_fin: fechaFin,
                    idSupervisor: idsupervisor,
                    idTienda: idtienda,
                    idPromotor: idpromotor,
                    idAgencia: idagencia
                };

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ReporteHorarioPromotores', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.obtenerReporteComisiones = function (fechaInicio, fechaFin, idsupervisor, idtienda, idpromotor) {
                var params = {
                    fecha_inicio: fechaInicio,
                    fecha_fin: fechaFin,
                    idSupervisor: idsupervisor,
                    idTienda: idtienda,
                    idPromotor: idpromotor
                };

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ReporteComisionPromotores', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.obtenerReporteNominas = function (fechaInicio, fechaFin, idsupervisor, idtienda, idpromotor, idagencia) {
                var params = {
                    fecha_inicio: fechaInicio,
                    fecha_fin: fechaFin,
                    idSupervisor: idsupervisor,
                    idTienda: idtienda,
                    idPromotor: idpromotor,
                    idAgencia: idagencia
                };

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ReporteNominaPromotores', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.obtenerReporteHorariosXls = function (fechaInicio, fechaFin, idsupervisor, idtienda, idpromotor, idagencia) {
                var params = {
                    fecha_inicio: fechaInicio,
                    fecha_fin: fechaFin,
                    idSupervisor: idsupervisor,
                    idTienda: idtienda,
                    idPromotor: idpromotor,
                    idAgencia: idagencia
                };

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ReporteHorarioPromotoresXls', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.obtenerReportePromo = function (cliente) {
                var params = {
                    cliente: cliente
                };

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ObtenerReportePromo', { params: params })
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            this.obtenerReportePedidos = function () {

                return $http.get(configuracion.DireccionActiva + 'Reportes/Web/ObtenerReportePedidos')
                    .then(function (resp) {
                        return $q.when(resp.data.Datos);
                    })
                    .catch(function (err) {
                        return $q.reject(err);
                    });
            };

            /*this.descargarReporteGenerado = function(guid, fileName) {
                $window.location = url + 'Reportes/xlsx/' + guid + (fileName ? '?fileName=' + fileName : '');
            };*/

        }
    ]);