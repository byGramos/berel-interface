'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.sweetalert
 * @description
 * # sweetalert
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
  .service('sweetAlert', ['SweetAlert', '$q','$window',
      function(SweetAlert, $q, $window) {

         this.success = function(titulo, mensaje) {
            return $q(function(exito) {
               SweetAlert.swal({
                  title: titulo,
                  text: mensaje,
                  type: 'success',
                  html:true
               }, function() {
                     $window.onkeydown = null;
                     $window.onfocus = null;
                  exito();
               });
            });
         };

         this.error = function(titulo, mensaje) {
            return $q(function(exito) {
               SweetAlert.swal({
                  title: titulo,
                  text: mensaje,
                  type: 'error',
               }, function() {
                  exito();
               });
            });
         };

         this.warning = function(titulo, mensaje) {
            return $q(function(exito) {
               SweetAlert.swal({
                  title: titulo,
                  text: mensaje,
                  type: 'warning',
               }, function() {
                  exito();
               });
            });
         };

         this.confirm = function(titulo, mensaje) {
            return $q(function(exito, error) {
               SweetAlert.swal({
                     title: titulo,
                     text: mensaje,
                     type: 'warning',
                     showCancelButton: true,
                     confirmButtonColor: '#DD6B55',
                     confirmButtonText: 'OK',
                     cancelButtonText: 'Cancelar',
                     closeOnConfirm: false
                  },
                  function(confirm) {
                     if (confirm) {
                        exito(confirm);
                     } else {
                        error(confirm);
                     }
                  });
            });
         };

         this.prompt = function(titulo, mensaje, inputDefaultValue){
            return $q(function(exito, error){
               SweetAlert.swal({
                     title: titulo,
                     text: mensaje,
                     type: 'input',
                     showCancelButton: true,
                     confirmButtonColor: '#DD6B55',
                     confirmButtonText: 'OK',
                     cancelButtonText: 'Cancelar',
                     closeOnConfirm: true,
                     animation: "slide-from-top",
                     inputPlaceholder: "Nombre de Reporte",
                     inputValue: inputDefaultValue
                  },
                  function(inputValue) {
                     if(inputValue === false){
                        error();
                        return false;
                     }
                     if(inputValue.trim() === ""){
                        SweetAlert.swal.showInputError("Tienes que escribir un nombre al reporte");
                        return false;
                     }

                     exito(inputValue);
                  });
            });
         };

         this.ayuda = function(titulo, mensaje) {
            return $q(function(exito) {
               SweetAlert.swal({
                  title: titulo,
                  text: mensaje,
                  closeOnEsc: true,
                  closeOnClickOutside: true
               }, function() {
                  exito();
               });
            });
         };



      }
   ]);

