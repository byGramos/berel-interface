(function() {

    'use strict';
    // jshint unused:false  http://localhost:9000
    //var serviceDEV = { IP: 'http://192.168.1.37/Services', Nombre: 'PC Berel668'};

    // Servicios Produccion Publicos
    var PUB = { IP: 'http://201.147.17.100:1085/BerelInterfaceServices', Nombre: 'IP PUBLICA'};
    var seguridadPUB = { IP: 'http://201.147.17.100:1085/SeguridadBerel', Nombre: 'Seguridad IP Publica'};

    // Servicios Produccion Intranet
    var PROD = { IP: 'http://192.168.1.21:1085/BerelInterfaceServices', Nombre: 'PRODUCCION'};
    var seguridadPROD = { IP: 'http://192.168.1.21:1085/SeguridadBerel', Nombre: 'Seguridad Berel'};
    
    // Servicios Locales
    var DEV = { IP: 'http://192.168.1.37/BerelInterfaceServices', Nombre: 'DESARROLLO'};    
    var seguridadDEV = { IP: 'http://192.168.1.37/SeguridadBerel', Nombre: 'Seguridad Berel'};
    
 

    var Servidor = DEV;
    var Seguridad = seguridadDEV;

    angular.module('berelWebApp')
        .constant('configuracion', {
            IDSistema: 5,
            DireccionActiva:  Servidor.IP + '/api/',
            DireccionLogin:  Seguridad.IP + '/api/',
            nombreServidor: Servidor.Nombre,
            urlNotificaciones: Servidor.IP + ':3000/',
            formatoFecha: 'YYYY-MM-DD',
            formatoFechaHora: 'YYYY-MM-DD HH:mm:ss',
            permisos: {
                inicio: 5041
            }
        });
})();