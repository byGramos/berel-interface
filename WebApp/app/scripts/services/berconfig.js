'use strict';

/**
 * @ngdoc service
 * @name berelWebApp.BERConfig
 * @description
 * # BERConfig
 * Service in the berelWebApp.
 */
angular.module('berelWebApp')
  .service('BERConfig',  ['$window', '$rootScope',
        function($window, $rootScope) {
          return {
                setPermisos: function(permisos,idPuesto) {
                    permisos.forEach(function(element) {                        
                        if(element.Opcion === 'Reporte Comisiones'){
                            $rootScope.RepComisionesVisible = element.Visible;                            
                        }                        
                        if(element.Opcion === 'Reporte Promo'){
                            $rootScope.RepoPromoVisible = element.Visible;                            
                        }
                        if(element.Opcion === 'Estatus Pedido Promo'){
                            $rootScope.ReportePedidoPromoVisible = element.Visible;                         
                        }
                        if(element.Opcion === 'Venta Home Depot'){
                            $rootScope.VentaHomeDepotVisible = element.Visible;                         
                        } 
                        if(element.Opcion === 'EstatusUUID'){
                            $rootScope.EstatusUUID = element.Visible;                         
                        }    
                        
                    }, this);
                    
                    // Si es administrador habilitamos todas la opciones RepoPromoVisible
                    if(idPuesto === 1){
                        $rootScope.RepComisionesVisible = true;
                        $rootScope.RepoPromoVisible = true;
                        $rootScope.ReportePedidoPromo = true;
                        $rootScope.VentaHomeDepot = true;
                        $rootScope.EstatusUUID = true;  
                    }
                },
                removePermisos: function(permisos) {
                    $rootScope.RepComisionesVisible = false;
                    $rootScope.RepNominasVisible = false;
                    $rootScope.RepoPromoVisible = false;
                    $rootScope.ReportePedidoPromo = false;
                    $rootScope.VentaHomeDepot = false;
                    $rootScope.EstatusUUID = false;  

                },
                setUsuarioActivo: function(usuario) {
                    $window.localStorage.setObject('usuarioActivo', usuario);
                    $rootScope.$broadcast('onLogin');
                },
                getUsuarioActivo: function() {
                    return $window.localStorage.getObject('usuarioActivo');
                },
                subsribeOnLogin: function(fn) {
                    $rootScope.$on('onLogin', fn);
                }
            };
    
  }
  ]);


