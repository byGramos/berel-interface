'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:KoneshCtrl
 * @description
 * # KoneshCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('KoneshCtrl',  ['$scope', '$rootScope', 'sweetAlert', 'NgTableParams', '$state', 'BERConfig', 'utils', 'reporte', 'catalogoGeneral',
  function ($scope, $rootScope, sweetAlert, NgTableParams, $state, BERConfig, utils, reporte, catalogoGeneral) {

    $scope.maxDate = moment().toDate();

    function ngTable(data) {
      $scope.tableParams = new NgTableParams({}, {
        dataset: data
      });
    }


    function cargarConsulta(resp) {
      angular.forEach(resp, function (value) {
        value.FechaEmision = moment(value.FechaEmision).format('YYYY-MM-DD');
        value.FechaRecepcion = moment(value.FechaRecepcion).format('YYYY-MM-DD');
        $scope.ocultarAceptado = value.Estado === 'ACEPTADO' ? true : false;
      });
      $scope.reporte = resp;
      $scope.reportePromo = true;

      if($scope.reporte === null){
        $scope.showAlert = true;
      }   
      
      if($scope.ocultarAceptado === true){
        $scope.ocultarRechazado = false;
      }
      else{
        $scope.ocultarRechazado = true;
      }

      ngTable(resp);
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.setRowSelected = function (index) {
      $scope.selectedRow = index;
    };


    $scope.consultarUUID = function () {
      $scope.showAlert = false;
      reporte.consultarUUID($scope.uuid)
        .then(function (data) {
          cargarConsulta(data);
        })
        .catch(function (err) {
          console.log(err);
          sweetAlert.error('Error');
          $scope.showAlert = true;
        });

    };

    $scope.obtenerAyuda = function(){
      var modulo='Monitoreo';

      utils.modalAyuda(modulo);
    }

 
    $scope.reporte = [];
    $scope.selectedRow = -1;
    $scope.ocultarAceptado = false;
    $scope.ocultarRechazado = false;

    //inicializarCombos();//obtenerSupervisores();  


    // funcion para exportar la informacion consultada
    $scope.getHeadersNgCsv = function () {
      var headers = [];
      var exclude = ['$$hashKey'];
      if($scope.reportePedidos === true){
        var excludeOwn = ['IDVisita','Linea','Tope','Litros','Estatus','Disponible'];
      }
      else{
        var excludeOwn = ['IDVisita','Pedido','Fecha','Ultimo','Siguiente','Peso'];
      }
      
      exclude = exclude.concat(excludeOwn);

      angular.forEach($scope.reporte[0], function (value, key) {
        headers.push(key);
      });

      return _.pullAll(headers, exclude);
    };

    $scope.getArrayNgCsv = function () {
      var array = [];

      angular.forEach($scope.reporte, function (value) {
        var obj = {};

        $scope.getHeadersNgCsv().forEach(function (h) {
          obj[h] = value[h];
          //Formateamos fechas
          if (_.startsWith(h.toLowerCase(), 'Fecha')) {
            obj[h] = moment(value[h]).format('YYYY-MM-DD');
          }
        });

        array.push(obj);
      });

      return array;
    };

    $scope.getFileNameNgCsvPedidos = function () {
      return 'Pedidos_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
    };

    $scope.getFileNameNgCsv = function () {
      return 'Cliente_'+ $scope.Cliente + '_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
    };


    ////////////////////////////////////////////////////////////////////////


  }
]);
