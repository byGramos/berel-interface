'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('MainCtrl',  
  function ($scope,$location,$rootScope,$state,$window,$timeout,BERConfig,
  autenticacion,sweetAlert) {

    $scope.menuHiden = true;
    $scope.menuShow = false;
    $scope.isLogged = false;
    $scope.bkgdVisible = false;

    //Obtiene el usuario Activo del localStorage si existe una sesion activa
    var loadUserData = function() {
      $scope.usuarioActivo = BERConfig.getUsuarioActivo();
    };


    BERConfig.subsribeOnLogin(function() {
      $scope.isLogged = autenticacion.isLogged();
      $scope.menuHiden = true;
      $scope.bkgdVisible = autenticacion.isLogged();

      if (autenticacion.isLogged()) {
        $scope.menuHiden = false;
        loadUserData();
        //opciones.obtenerOpciones(true);
      }
    });

    //BERConfig.subsribeSucursalChange(loadSucursal);

    loadUserData();
    //loadSucursal();

    //Metodo para cerrar la sesion del usuario activo
    $scope.cerrarSesion = function() {
        autenticacion.signOut()
            .then(function() {
                document.getElementById("mySidenav").style.width = "0";
                document.getElementById("main").style.marginLeft = "0";
                $location.path('/login');
            })
            .catch(function(err) {
                sweetAlert.error('Error', err, 'error');
            });
    };

    //Mostrar / ocultar Menu
    $scope.ocultarMostrarMenu = function() {
        if (autenticacion.isLogged()) {
            $scope.menuHiden = !$scope.menuHiden;

            $scope.$broadcast('show-menu-before', !$scope.menuHiden);
            $timeout(function() {
                $scope.$broadcast('show-menu', !$scope.menuHiden);
            }, 200);
        }
    };

    //Metodo para validar si existe una sesion activa, sino envia a vista login.
    autenticacion.checkAutoKeepAlive(function(isLogged) {
                $scope.isLogged = isLogged;
                $scope.bkgdVisible = isLogged;
                if (!isLogged) {
                    $state.go('login');
                }
            });
    
    $scope.ocultarMostrarMenu();

    }
  );
