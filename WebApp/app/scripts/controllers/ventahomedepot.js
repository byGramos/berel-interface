'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:VentahomedepotCtrl
 * @description
 * # VentahomedepotCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('VentahomedepotCtrl', ['$scope', '$rootScope', 'sweetAlert', 'NgTableParams', '$state', 'BERConfig', 'utils', 'reporte', 'catalogoGeneral',
    function ($scope, $rootScope, sweetAlert, NgTableParams, $state, BERConfig, utils, reporte, catalogoGeneral) {

      $scope.tituloPagina = 'VentaHomeDepot';
      $scope.UltimaDescarga = "";

      function ngTable(data) {
        $scope.tableParams = new NgTableParams({}, {
          dataset: data
        });
      }

      function obtenerUltimaDescarga() {
        reporte.obtenerUltimaDescarga()
          .then(function (resp) {
            if (resp) {
              var tmp = resp.split('|');
              $scope.UltimaDescarga = moment(tmp[0]).format('DD-MM-YYYY');
              $scope.Divisor = tmp[1];
            } else {
              sweetAlert.warning('Atención', 'No se encontro info. Contacte a sistemas.');
            }
          })
          .catch(function (resp) {
            console.log(resp);
          });

      };

      function cargarConsulta(resp,tipo) {
        if(tipo === 1){
          $scope.reporte = resp;
        }
        else{
          $scope.reporte2 = resp;
        }
        

        //ngTable(resp);
      };




      //////////////////////////////////////////////////////////////////////////////////////////////////////////

      $scope.setRowSelected = function (index) {
        $scope.selectedRow = index;
      };


      $scope.obtenerReporte = function (tipo) {
        reporte.obtenerVentaHomeDepot(tipo)
          .then(function (resp) {
            if (resp) {
              cargarConsulta(resp,tipo);
              //reporte.descargarReporteGenerado(resp, 'ReporteComisiones.xlsx');
            } else {
              sweetAlert.warning('Atención', 'No se encontró información en este periodo de fechas.');
            }
          })
          .catch(function (err) {
            console.log(err);
            sweetAlert.error('Error');
          });
      };

      $scope.bajarReporte = function (tipo) {
        if (tipo === 1) {
          reporte.descargarReporteGenerado($scope.reporte, 'VentaSelloutHD_' + moment().format('YYYYMMDD') + '.xlsx');
        }
        else {
          reporte.descargarReporteGenerado($scope.reporte2, 'VentaConsolidadaHD' + moment().format('YYYYMMDD') + '.xlsx');
        }

      };

      $scope.reporte = [];
      $scope.reporte2 = [];
      obtenerUltimaDescarga()

      // funcion para exportar la informacion consultada
      $scope.getHeadersNgCsv = function () {
        var headers = [];
        var exclude = ['$$hashKey'];
        var excludeOwn = ['IDVisita'];
        exclude = exclude.concat(excludeOwn);

        angular.forEach($scope.reporte[0], function (value, key) {
          headers.push(key);
        });

        return _.pullAll(headers, exclude);
      };

      $scope.getArrayNgCsv = function () {
        var array = [];

        angular.forEach($scope.reporte, function (value) {
          var obj = {};

          $scope.getHeadersNgCsv().forEach(function (h) {
            obj[h] = value[h];
            //Formateamos fechas
            if (_.startsWith(h.toLowerCase(), 'Fecha')) {
              obj[h] = moment(value[h]).format('YYYY-MM-DD');
            }
          });

          array.push(obj);
        });

        return array;
      };

      $scope.getFileNameNgCsv = function () {
        return $scope.tituloPagina + '_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
      };

      ////////////////////////////////////////////////////////////////////////


    }
  ]);

