'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:ReportecomisionesCtrl
 * @description
 * # ReportecomisionesCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('ReportecomisionesCtrl', ['$scope','$rootScope', 'sweetAlert', 'NgTableParams', '$state', 'BERConfig','utils','reporte','catalogoGeneral',
  function ($scope, $rootScope, sweetAlert, NgTableParams, $state, BERConfig,utils,reporte,catalogoGeneral) {

   $scope.tituloPagina = 'ReporteComisiones';

   $scope.esActualizado = false;
   $scope.existsPromotores = false;
   
   
   $scope.filtros = {
    fechaInicio: moment().toDate(),
    fechaFin: moment().toDate(),
    supervisores: {},
    tiendas:{},
    promotores:{}
  };
  $scope.maxDate = moment().toDate();

    function ngTable(data) {
      $scope.tableParams = new NgTableParams({}, {
        dataset: data
      });
    }

    function inicializarCombos() {  
      
      $scope.supervisores = [{
        ID: -1,
        Descripcion: 'Todos'
      }];

      $scope.tiendas = [{
        ID: -1,
        Descripcion: 'Todos'
      }];

      $scope.promotores = [{
        ID: -1,
        Descripcion: 'Todos'
      }];

    }

    function obtenerSupervisores() {
      inicializarCombos();
       $scope.usuarioActivo = BERConfig.getUsuarioActivo();
      catalogoGeneral.ObtenerSupervisores()
        .then(function (resp) {
          if (resp) {
            $scope.supervisores = $scope.supervisores.concat(resp);
            $scope.filtros.supervisores.ID = $scope.usuarioActivo.IDUsuario;
            $scope.filtros.supervisores.Descripcion = $scope.usuarioActivo.Nombre +'' + $scope.usuarioActivo.Apellidos;
          } else {
            sweetAlert.warning('Atención', 'No se encontraron Supervisores. Contacte a sistemas.');
          }

          $scope.filtros.supervisores = $scope.supervisores[0];

          cargarTiendas($scope.filtros.supervisores);
        })
        .catch(function (resp) {
          console.log(resp);
        });

    }

    function cargarTiendas(supervisor){
        catalogoGeneral.obtenerTiendasAsignadas(supervisor.ID)
        .then(function (resp) {
          if (resp) {
            $scope.tiendas = [{ ID: -1,Descripcion: 'Todos' }];
            $scope.tiendas =  $scope.tiendas.concat(resp);//resp;
            if (resp.length > 0) {
            $scope.filtros.tiendas = $scope.tiendas[0];
            cargarPromotores($scope.filtros.tiendas);
          }
          } else {
            sweetAlert.warning('Atención', 'No se encontraron Tiendas. Contacte a sistemas.');
          }
        })
        .catch(function (resp) {
          console.log(resp);
          $scope.filtros.tiendas = $scope.tiendas[0]; 
          $scope.filtros.promotores = $scope.promotores[0];
        });
    }

    function cargarPromotores(tienda){
      
        catalogoGeneral.obtenerPromotoresByTienda(tienda.ID)
        .then(function (resp) {
          if (resp) {
            $scope.filtros.promotores = $scope.promotores[0];
            if (resp.length > 0) {
              $scope.promotores = $scope.promotores.concat(resp);
              $scope.filtros.promotores = $scope.promotores[0];
              $scope.existsPromotores = true;
            }
          } else {
            sweetAlert.warning('Atención', 'No se encontraron Promotores. Contacte a sistemas.');
          }
        })
        .catch(function (resp) {
          console.log(resp);
          $scope.filtros.promotores = $scope.promotores[0]; 
        });
    }


    function cargarConsulta(resp) {
      /*angular.forEach(resp, function (value) {
        value.FechaIngreso = moment(value.FechaIngreso).format('YYYY-MM-DD');
        value.FechaBaja = moment(value.FechaBaja).format('YYYY-MM-DD');
      });*/
      $scope.reporte = resp;

      //ngTable(resp);
    }

 
    

//////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.setRowSelected = function(index){
      $scope.selectedRow = index;
    };

    $scope.obtenerTiendas = function(supervisor){
      cargarTiendas(supervisor);
    }

    $scope.obtenerPromotores = function(tienda){
        cargarPromotores(tienda);
    }

    $scope.obtenerReporte = function () {
      reporte.obtenerReporteComisiones($scope.filtros.fechaInicio, $scope.filtros.fechaFin,$scope.filtros.supervisores.ID,$scope.filtros.tiendas.ID,$scope.filtros.promotores.ID)
        .then(function (resp) {
          if (resp) {
            cargarConsulta(resp);
            //reporte.descargarReporteGenerado(resp, 'ReporteComisiones.xlsx');
          } else {
            sweetAlert.warning('Atención', 'No se encontró información en este periodo de fechas.');
          }
        })
        .catch(function (err) {
          console.log(err);
          sweetAlert.error('Error');
        });
    };

   $scope.bajarReporte = function(){
    reporte.descargarReporteGenerado($scope.reporte, 'ReporteComisiones.xlsx');
   };

    $scope.filtro = {};
    $scope.reporte = [];
    $scope.selectedRow = -1;      
    
    obtenerSupervisores();  


    $scope.openCalendar = function (calendario) {
      if (calendario === 1) {
        $scope.openedFechaInicio = true;
        $scope.openedFechaFin = !$scope.openedFechaInicio;
      } else {
        $scope.openedFechaInicio = false;
        $scope.openedFechaFin = !$scope.openedFechaInicio;
      }
    };

    
    // funcion para exportar la informacion consultada
    $scope.getHeadersNgCsv = function () {
      var headers = [];
      var exclude = ['$$hashKey'];
      var excludeOwn = ['IDVisita'];
      exclude = exclude.concat(excludeOwn);

      angular.forEach($scope.reporte[0], function (value, key) {
        headers.push(key);
      });

      return _.pullAll(headers, exclude);
    };

    $scope.getArrayNgCsv = function () {
      var array = [];

      angular.forEach($scope.reporte, function (value) {
        var obj = {};

        $scope.getHeadersNgCsv().forEach(function (h) {
          obj[h] = value[h];
          //Formateamos fechas
          if (_.startsWith(h.toLowerCase(), 'Fecha')) {
            obj[h] = moment(value[h]).format('YYYY-MM-DD');
          }
        });

        array.push(obj);
      });

      return array;
    };

    $scope.getFileNameNgCsv = function () {
      return $scope.tituloPagina + '_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
    };

    ////////////////////////////////////////////////////////////////////////


}
]);

