'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:ReportepromoCtrl
 * @description
 * # ReportepromoCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('ReportepromoCtrl', ['$scope', '$rootScope', 'sweetAlert', 'NgTableParams', '$state', 'BERConfig', 'utils', 'reporte', 'catalogoGeneral',
    function ($scope, $rootScope, sweetAlert, NgTableParams, $state, BERConfig, utils, reporte, catalogoGeneral) {


      $scope.esActualizado = false;
      $scope.existsPromotores = false;
      

      /*$scope.filtros = {
       Cliente: ''
      };*/

      $scope.maxDate = moment().toDate();

      function ngTable(data) {
        $scope.tableParams = new NgTableParams({}, {
          dataset: data
        });
      }


      function cargarConsulta(resp) {
        angular.forEach(resp, function (value) {
          //value.Fecha = moment(value.Fecha).format('YYYY-MM-DD');
          value.Cubetas = Math.round(value.Disponible / 19);
          value.Galones = Math.round(value.Disponible / 4);
        });
        $scope.reporte = resp;
        $scope.reportePromo = true;

        if($scope.reporte.length === 0){
          $scope.showAlert = true;
        }
        

        ngTable(resp);
      }

      function cargarConsultaPedidos(resp) {
        angular.forEach(resp, function (value) {
          value.Fecha = moment(value.Fecha).format('YYYY-MM-DD');
        });
        $scope.reporte = resp;
        $scope.reportePedidos = true;

        if($scope.reporte.length === 0){
          $scope.showAlert = true;
        }

        ngTable(resp);
      }




      //////////////////////////////////////////////////////////////////////////////////////////////////////////

      $scope.setRowSelected = function (index) {
        $scope.selectedRow = index;
      };


      $scope.obtenerReporte = function () {
        $scope.showAlert = false;
        reporte.obtenerReportePromo($scope.Cliente)
          .then(function (data) {
            cargarConsulta(data);
          })
          .catch(function (err) {
            console.log(err);
            sweetAlert.error('Error');
            $scope.showAlert = true;
          });

      };

      $scope.obtenerReportePedidos = function () {
        $scope.showAlert = false;
        reporte.obtenerReportePedidos()
          .then(function (data) {
            cargarConsultaPedidos(data);
          })
          .catch(function (err) {
            console.log(err);
            sweetAlert.error('Error');
            $scope.showAlert = true;
          });

      };

    
      $scope.reporte = [];
      $scope.selectedRow = -1;

      //inicializarCombos();//obtenerSupervisores();  


      // funcion para exportar la informacion consultada
      $scope.getHeadersNgCsv = function () {
        var headers = [];
        var exclude = ['$$hashKey'];
        if($scope.reportePedidos === true){
          var excludeOwn = ['IDVisita','Linea','Tope','Litros','Estatus','Disponible'];
        }
        else{
          var excludeOwn = ['IDVisita','Pedido','Fecha','Ultimo','Siguiente','Peso'];
        }
        
        exclude = exclude.concat(excludeOwn);

        angular.forEach($scope.reporte[0], function (value, key) {
          headers.push(key);
        });

        return _.pullAll(headers, exclude);
      };

      $scope.getArrayNgCsv = function () {
        var array = [];

        angular.forEach($scope.reporte, function (value) {
          var obj = {};

          $scope.getHeadersNgCsv().forEach(function (h) {
            obj[h] = value[h];
            //Formateamos fechas
            if (_.startsWith(h.toLowerCase(), 'Fecha')) {
              obj[h] = moment(value[h]).format('YYYY-MM-DD');
            }
          });

          array.push(obj);
        });

        return array;
      };

      $scope.getFileNameNgCsvPedidos = function () {
        return 'Pedidos_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
      };

      $scope.getFileNameNgCsv = function () {
        return 'Cliente_'+ $scope.Cliente + '_' + moment().format('YYYYMMDD') + '_' + moment().format('HHmm');
      };


      ////////////////////////////////////////////////////////////////////////


    }
  ]);
