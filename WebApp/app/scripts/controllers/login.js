'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('LoginCtrl', ['$scope', 'autenticacion', 'sweetAlert', 'BERConfig', '$location', '$rootScope',
        function($scope, autenticacion, sweetAlert, BERConfig, $location, $rootScope) {

            $scope.login = function() {
                autenticacion.signIn($scope.login.Usuario, $scope.login.Password)
                    .then(function(user) {
                        var forward = function() {
                            $location.path($rootScope.returnToState || '/inicio');
                        };

                        if(user){
                            BERConfig.setUsuarioActivo(user);
                            BERConfig.setPermisos(user.Permisos,user.IDPuesto);
                            forward();
                        }
                        else {
                            sweetAlert.error('Error', 'Error');
                        }

                    })
                    .catch(function(err) {
                        sweetAlert.error('Error', err);
                    });
            };

            $scope.entrar = function(evento) {
                if (evento.which === 13) {
                    $scope.login();
                }
            };

            $scope.siguiente = function(evento) {
                if (evento.which === 13) {
                    $('#txtPassword').focus();
                }
            };

        }
    ]);

