'use strict';

/**
 * @ngdoc function
 * @name berelWebApp.controller:InicioCtrl
 * @description
 * # InicioCtrl
 * Controller of the berelWebApp
 */
angular.module('berelWebApp')
  .controller('InicioCtrl',  ['$scope', 'BERConfig', 'configuracion',
        function($scope, BERConfig, configuracion) {

            //$scope.tienePermiso = !!opciones.tienePermiso(configuracion.permisos.inicio);

            $scope.filtros = {
                fechaInicio: moment().toDate(),
                fechaFin: moment().toDate()
            };
            $scope.hayInformacionIndicadores = false;
            $scope.fechaActual = moment().format('dddd D [de] MMMM [de] YYYY');

             var usuarioActivo = BERConfig.getUsuarioActivo();


        }
    ]);
